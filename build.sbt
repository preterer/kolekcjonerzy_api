name := """template-play-java"""

version := "1.0-SNAPSHOT"

resolvers += Resolver.url("Objectify Play Repository", url("http://schaloner.github.io/releases/"))(Resolver.ivyStylePatterns)

resolvers += Resolver.url("Objectify Play Snapshot Repository", url("http://schaloner.github.io/snapshots/"))(Resolver.ivyStylePatterns)

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "be.objectify" %% "deadbolt-java" % "2.3.1",
  "org.apache.commons" % "commons-email" % "1.3.3",
  "info.cukes" % "cucumber-java" % "1.1.5" % "test", 
  "info.cukes" % "cucumber-junit" % "1.1.5" % "test",
  "junit" % "junit" % "4.11" % "test" exclude("org.hamcrest", "hamcrest-core"), //jesli tego nie bedzie, popsuje się hamcrest. bazuje na innej wersji JUNIT
  "org.mockito" % "mockito-all" % "1.9.5" % "test",
  "org.hamcrest" % "hamcrest-all" % "1.3" % "test",
  "org.jvnet.mock-javamail" % "mock-javamail" % "1.9" % "test",
  //JWT
  "com.auth0" % "java-jwt" % "2.1.0",
  "mysql" % "mysql-connector-java" % "5.1.37",
  "com.wordnik" %% "swagger-play2" % "1.3.12" exclude("org.reflections", "reflections"),
  "org.reflections" % "reflections" % "0.9.8" notTransitive ()
)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "features")

