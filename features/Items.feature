Feature: Collection items
  Scenario: User wants to add an item to an existing collection when authorized
    Given an admin account
    Given a collection named "My collection"
    Given a token for requests
    When I try to add an item to the collection named "My collection" with token
    Then I get ok

  Scenario: User wants to add an item to an existing collection when unauthorized
    Given an admin account
    Given a collection named "My collection"
    When I try to add an item to the collection named "My collection" without token
    Then I get forbidden

  Scenario: User wants to add an item to a non-existing collection when authorized
    Given an admin account
    Given a collection named "My collection"
    Given a token for requests
    When I try to add an item to the collection named "Not my collection" with token
    Then I get notFound

  Scenario: User wants to remove an existing item when authorized
    Given an admin account
    Given a token for requests
    Given a collection "My collection" containing item named "Item" with url "http://item"
    When I try to remove the item "Item" from "My collection" with token
    Then I get noResponse
    Then The item "Item" is removed from "My collection"

  Scenario: User wants to remove an existing item when unauthorized
    Given an admin account
    Given a collection "My collection" containing item named "Item" with url "http://item"
    When I try to remove the item "Item" from "My collection" without token
    Then I get forbidden
    Then The item "Item" is not removed from "My collection"

  Scenario: User wants to remove a non-existing item when authorized
    Given an admin account
    Given a token for requests
    When I try to remove an item which doesn't exist with token
    Then I get notFound

  Scenario: User wants to remove a non-existing item when authorized
    Given an admin account
    When I try to remove an item which doesn't exist without token
    Then I get forbidden

  Scenario: User wants to move an item
    Given an admin account
    Given a token for requests
    Given a collection "My collection" containing item named "Item" with url "http://item"
    Given a collection named "Other collection"
    When I try to move item "Item" from "My collection" to "Other collection"
    Then I get ok
    Then Item "Item" is in collection "Other collection"
    Then Item "Item" is not in collection "My collection"

  Scenario: User wants to change items information
    Given an admin account
    Given a token for requests
    Given a collection "My collection" containing item named "Item" with url "http://item"
    When I try to change items "Item" from collection "My collection" name to "ItemName" and link to "image-Url"
    Then I get ok
    Then Item "ItemName" is in collection "My collection"
    Then Item "Item" is not in collection "My collection"