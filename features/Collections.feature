Feature: Collections

  Scenario: User wants to get list of collections when authorized
    Given an admin account
    Given a token for requests
    Given a collections list
    When I try to get collections list with token
    Then I get the list

  Scenario: User wants to get list of collections when unauthorized
    Given an admin account
    Given a collections list
    When I try to get collections list without token
    Then I get forbidden

  Scenario: User wants to get details of a collection when authorized
    Given an admin account
    Given a token for requests
    Given a collection named "My Collection no1"
    When I try to get a collection named "My Collection no1" with token
    Then I get ok

  Scenario: User wants to get details of a collection when unauthorized
    Given an admin account
    Given a collection named "My collection no1"
    When I try to get a collection named "My Collection no1" without token
    Then I get forbidden

  Scenario: User wants to add a collection which doesn't contain items when authorized
    Given an admin account
    Given a token for requests
    When I try to add a collection named "My new collection" with token
    Then I get ok

  Scenario: User wants to add a collection which doesn't contain items when unauthorized
    Given an admin account
    When I try to add a collection named "My new collection" without token
    Then I get forbidden

  Scenario: User wants to add a collection which containts items when authorized
    Given an admin account
    Given a token for requests
    When I try to add a collection named "My new collection 2" with item containing name "Item" and url "1" with token
    Then I get ok

  Scenario: User wants to add a collection which containts items when unauthorized
    Given an admin account
    When I try to add a collection named "My new collection 2" with item containing name "Item" and url "1" without token
    Then I get forbidden

  Scenario: User wants to remove an existing collection when authorized
    Given an admin account
    Given a collection named "My collection"
    Given a token for requests
    When I try to remove "My collection" with token
    Then I get noResponse
    Then The collection "My collection" is removed

  Scenario: User wants to remove an existing collection when unauthorized
    Given an admin account
    Given a collection named "My collection"
    When I try to remove "My collection" without token
    Then I get forbidden
    Then The collection "My collection" is not removed

  Scenario: User wants to remove a collection which doesn't exist when authorized
    Given an admin account
    Given a token for requests
    When I try to remove the non-existing collection with token
    Then I get notFound

  Scenario: User wants to remove a collection which doesn't exist when unauthorized
    Given an admin account
    When I try to remove the non-existing collection without token
    Then I get forbidden

  Scenario: User wants to change his collections name
    Given an admin account
    Given a collection named "My collection"
    Given a token for requests
    When I try to change collections name from "My collection" to "Another collection name"
    Then I get ok
    Then There is a collection "Another collection name"
    Then There is no collection "My collection"

  Scenario: User wants to get some random collections
    Given an admin account
    Given 10000 collections
    When I try to get random collections
    Then I get ok
    Then I get random collections