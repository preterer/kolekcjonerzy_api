Feature: Friends
  Scenario: User wants to invite another user to his friends list
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "other@user.pl" and password "password"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I invite "other@user.pl"
    Then I get noResponse
    Then user "user@user.pl" has invitation to "other@user.pl"
    Then user "other@user.pl" has invitation from "user@user.pl"

  Scenario: User wants to accept an invitation
    Given an user "User" "Usersky" with email "fromAccept@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "toAccept@user.pl" and password "password"
    Given an invitation from user "fromAccept@user.pl" to "toAccept@user.pl"
    Given a token which belongs to user with email "toAccept@user.pl" and password "password"
    When I as "toAccept@user.pl" accept invitation from "fromAccept@user.pl"
    Then I get noResponse
    Then user "fromAccept@user.pl" has user "toAccept@user.pl" in his friends list
    Then user "toAccept@user.pl" has user "fromAccept@user.pl" in his friends list

  Scenario: User wants to reject an invitation
    Given an user "User" "Usersky" with email "fromRejection@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "toRejection@user.pl" and password "password"
    Given an invitation from user "fromRejection@user.pl" to "toRejection@user.pl"
    Given a token which belongs to user with email "toRejection@user.pl" and password "password"
    When I as "toRejection@user.pl" reject invitation from "fromRejection@user.pl"
    Then I get noResponse
    Then user "fromRejection@user.pl" doesn't have user "toRejection@user.pl" in his friends list
    Then user "toRejection@user.pl" doesn't have user "fromRejection@user.pl" in his friends list

  Scenario: User wants to see his friends requests list
    Given an user "User" "Usersky" with email "mainList@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "list2@user.pl" and password "password"
    Given an user "Another" "Usersky" with email "list3@user.pl" and password "password"
    Given an invitation from user "mainList@user.pl" to "list2@user.pl"
    Given an invitation from user "list3@user.pl" to "mainList@user.pl"
    Given a token which belongs to user with email "mainList@user.pl" and password "password"
    When I ask for my friends requests list
    Then I get ok
    Then I get list of requests for "mainList@user.pl"

  Scenario: User wants to see his friends requests list with limit
    Given an user "User" "Usersky" with email "mainList@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "list2@user.pl" and password "password"
    Given an user "Another" "Usersky" with email "list3@user.pl" and password "password"
    Given an invitation from user "mainList@user.pl" to "list2@user.pl"
    Given an invitation from user "list3@user.pl" to "mainList@user.pl"
    Given a token which belongs to user with email "mainList@user.pl" and password "password"
    When I ask for my friends requests list with limit 10 and start 0
    Then I get ok
    Then I get list of requests for "mainList@user.pl"