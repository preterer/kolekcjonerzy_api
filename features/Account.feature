Feature: Account management
  Scenario: User wants to get details of his account
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I ask for information about me
    Then I get ok
    Then I get information about user "user@user.pl"

  Scenario: User wants to get details of someone else's account
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "other@user.pl" and password "password"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I ask for information about user "other@user.pl"
    Then I get ok
    Then I get information about user "other@user.pl"

  Scenario: User wants to change his data
    Given an user "Test" "Testowsky" with email "changeUserData@user.pl" and password "password"
    Given a token which belongs to user with email "changeUserData@user.pl" and password "password"
    When I try to change my name to "Name" and last name to "Last Name"
    Then I get ok
    Then User "changeUserData@user.pl" has name "Name" and last name "Last Name"

  Scenario: User wants to change his password
    Given an user "Test" "Testowsky" with email "changeUserPassword@user.pl" and password "password"
    Given a token which belongs to user with email "changeUserPassword@user.pl" and password "password"
    When I try to change my password from "password" to "Passw0rd"
    Then I get noResponse
    Then Password for user "changeUserPassword@user.pl" is set to "Passw0rd"

  Scenario: User wants to close his account
    Given an user "Test" "Testowsky" with email "disableUser@user.pl" and password "password"
    Given a token which belongs to user with email "disableUser@user.pl" and password "password"
    When I try to disable my account
    Then I get noResponse
    Then The account "disableUser@user.pl" is disabled