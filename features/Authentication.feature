Feature: Authentication

    Scenario: Admin wants to sign in successfully
        Given an admin account
        When I sign in with email "admin@admin.pl" and password "password"
        Then I get success response and auth-token

    Scenario: Admin wants to sign in unsuccessfully
        Given an admin account
        When I sign in with email "admin@admin.pl" and password "aaaa"
        Then I get unauthorized

    Scenario: User wants to sign up using his email
        When I sign up with email "accountCreationTest@user.pl", password "password", name "John" and last name "Lock"
        Then I get success

    Scenario: User wants to sign up using admins email
        Given an admin account
        When I sign up with email "admin@admin.pl", password "password", name "John" and last name "Lock"
        Then I get forbidden