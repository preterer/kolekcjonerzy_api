Feature: Trades
  Scenario: User wants to start a trade with another user
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "other@user.pl" and password "password"
    Given a collection "My collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Not my collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I offer a trade of item "Item" belonging to "My collection" and user "user@user.pl" for item "Item1" belonging to "Not my collection" and user "other@user.pl"
    Then I get ok

  Scenario: User wants to start a trade with self
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given a collection "My collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Not my collection" containing item named "Item1" with url "http://item1" which belongs to user "user@user.pl"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I offer a trade of item "Item" belonging to "My collection" and user "user@user.pl" for item "Item1" belonging to "Not my collection" and user "user@user.pl"
    Then I get badRequest

  Scenario: User wants to accept a trade
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "other@user.pl" and password "password"
    Given a collection "Accept collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Not my collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a collection "Different collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Yet another collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Accept collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Different collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Yet another collection" for item "Item" from collection "Accept collection" owned by user "user@user.pl"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I try to accept the trade between items "Item1" from collection "Not my collection" belonging to user "other@user.pl" and item "Item" from collection "Accept collection" belonging to user "user@user.pl"
    Then I get noResponse
    #Users and collections are swapped, since the trade has been accepted
    Then Other trades of item "Item" from collection "Not my collection" belonging to user "other@user.pl" are gone
    Then Other trades of item "Item1" from collection "Accept collection" belonging to user "user@user.pl" are gone

  Scenario: User wants to reject a trade
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "other@user.pl" and password "password"
    Given a collection "Reject collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Not my collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a collection "Different collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Yet another collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Reject collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Different collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Yet another collection" for item "Item" from collection "Reject collection" owned by user "user@user.pl"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I try to reject the trade between items "Item1" from collection "Not my collection" belonging to user "other@user.pl" and item "Item" from collection "Reject collection" belonging to user "user@user.pl"
    Then I get noResponse
    Then Other trades of item "Item" from collection "Reject collection" belonging to user "user@user.pl" are remaining
    Then Other trades of item "Item1" from collection "Not my collection" belonging to user "other@user.pl" are remaining

  Scenario: User wants to see his offers
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "other@user.pl" and password "password"
    Given a collection "Accept collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Not my collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a collection "Different collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Yet another collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Accept collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Different collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Yet another collection" for item "Item" from collection "Accept collection" owned by user "user@user.pl"
    Given a token which belongs to user with email "user@user.pl" and password "password"
    When I try to get my offers
    Then I get ok
    Then I get some trades

  Scenario: User wants to see his requests
    Given an user "User" "Usersky" with email "user@user.pl" and password "password"
    Given an user "Other" "Usersky" with email "other@user.pl" and password "password"
    Given a collection "Accept collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Not my collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a collection "Different collection" containing item named "Item" with url "http://item" which belongs to user "user@user.pl"
    Given a collection "Yet another collection" containing item named "Item1" with url "http://item1" which belongs to user "other@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Accept collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Not my collection" for item "Item" from collection "Different collection" owned by user "user@user.pl"
    Given a trade from user "other@user.pl" offering item "Item1" from collection "Yet another collection" for item "Item" from collection "Accept collection" owned by user "user@user.pl"
    Given a token which belongs to user with email "other@user.pl" and password "password"
    When I try to get my requests
    Then I get ok
    Then I get some trades