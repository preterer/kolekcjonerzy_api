package helpers;

import dto.in.SignInJson;
import models.Account;
import models.Role;
import org.hamcrest.core.Is;
import org.junit.Assert;
import play.libs.Json;
import play.mvc.Result;
import play.test.FakeRequest;
import utils.Constants;

import java.util.Map;

import static play.test.Helpers.*;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class GlobalHelpers {
    public static final GlobalHelpers INSTANCE = new GlobalHelpers();
    private GlobalHelpers(){}

    private Account adminAccount = Account.FINDER.where().eq("email", "admin@admin.pl").findUnique();
    public Result result;
    public String token;

    private Account createAdminAccount() {
        Account account = new Account();
        account.email = "admin@admin.pl";
        account.password = "password";
        account.name = "Joe";
        account.lastName = "Doe";
        account.roles.add(Role.findByName("admin"));
        account.save();
        return account;
    }

    public Account getAdminAccount() {
        if(adminAccount == null){
            adminAccount = createAdminAccount();
        }
        return adminAccount;
    }

    public void getToken() {
        getToken("admin@admin.pl", "password");
    }

    public void getToken(String email, String password) {
        SignInJson signInJson = new SignInJson();
        signInJson.setEmail(email);
        signInJson.setPassword(password);
        routeWithoutToken(fakeRequest("POST", "/signIn").withJsonBody(Json.toJson(signInJson)));
        assertTokenIsPresent();
    }

    public void routeWithoutToken(FakeRequest fakeRequest) {
        result = route(fakeRequest);
    }

    public void routeWithToken(FakeRequest fakeRequest) {
        result = route(fakeRequest.withHeader(Constants.AUTH_TOKEN_HEADER, token));
    }

    public void assertResponse(Integer status) {
        Assert.assertThat(status(result), Is.is(status));
    }

    private Map<String, String> getHeaders() {
        return headers(result);
    }

    public void assertTokenIsPresent(){
        Assert.assertTrue(getHeaders().containsKey(Constants.AUTH_TOKEN_HEADER));
        token = getHeaders().get(Constants.AUTH_TOKEN_HEADER);
    }
}
