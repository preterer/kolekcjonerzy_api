package helpers;

import models.Account;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 * Created by ja on 26.11.15.
 */
public class AuthHelpers {
    public static final AuthHelpers INSTANCE = new AuthHelpers();

    private AuthHelpers() {
        super();
    }

    public Account createAccount(String email, String password, String name, String lastName) {
        Account account = new Account();
        account.active = true;
        account.email = email;
        account.password = password;
        account.name = name;
        account.lastName = lastName;
        account.save();
        return account;
    }

    public void assertAccountExists(String email, String password, String name, String lastName) {
        Account account = Optional.ofNullable(Account.findByEmailActive(email)).orElseGet(() -> createAccount(email, password, name, lastName));
        assertEquals(account.email, email);
    }
}
