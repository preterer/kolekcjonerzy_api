package helpers;

import models.CollectionItem;
import models.Trade;

import java.util.Optional;

/**
 * Created by ja on 06.01.16.
 */
public class TradesHelper {
    public static final TradesHelper INSTANCE = new TradesHelper();

    private TradesHelper() {
    }

    public Trade createTrade(CollectionItem offeredItem, CollectionItem wantedItem) {
        Trade trade = new Trade();
        trade.message = "Message";
        trade.offeredItem = offeredItem;
        trade.wantedItem = wantedItem;
        trade.save();
        return trade;
    }

    public void assertTradeExists(CollectionItem offeredItem, CollectionItem wantedItem) {
        Optional.ofNullable(Trade.findByTradeItemsActive(offeredItem.id, wantedItem.id)).orElseGet(() -> createTrade(offeredItem, wantedItem));
    }
}
