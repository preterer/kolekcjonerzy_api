package helpers;

import com.google.common.collect.Lists;
import models.Account;
import models.CollectionItem;
import models.MyCollection;
import org.junit.Assert;

import java.util.Optional;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class CollectionHelpers {
    public static final CollectionHelpers INSTANCE = new CollectionHelpers();
    private CollectionHelpers(){};

    public void createCollections(Integer collectionsLeft){
        createCollection("Collection no" + collectionsLeft);
        if(collectionsLeft != 0){
            createCollections(collectionsLeft - 1);
        }
    }

    public MyCollection createCollection(String collectionName){
        return createCollection(collectionName, Account.findByEmailActive("admin@admin.pl"));
    }

    public MyCollection createCollection(String collectionName, Account account) {
        MyCollection myCollection = new MyCollection();
        myCollection.name = collectionName;
        myCollection.account = account;
        myCollection.collectionItems = Lists.newArrayList();
        myCollection.save();
        return myCollection;
    }

    public void assertThatCollectionExists(String collectionName) {
        assertThatCollectionExistsForUser(collectionName, "admin@admin.pl");
    }

    public void assertThatCollectionExistsForUser(String collectionName, String email) {
        Optional.ofNullable(Account.findByEmailActive(email))
                .map(account -> MyCollection.findByNameAndUserIdActive(collectionName, account.id))
                .orElseGet(() -> createCollection(collectionName, Account.findByEmailActive(email)));
        Assert.assertNotNull(MyCollection.findByNameAndEmailActive(collectionName, email));
    }

    public CollectionItem createItem(String itemName, String imageUrl, MyCollection myCollection) {
        CollectionItem collectionItem = new CollectionItem();
        collectionItem.account = myCollection.account;
        collectionItem.imageUrl = imageUrl;
        collectionItem.myCollection = myCollection;
        collectionItem.name = itemName;
        collectionItem.save();
        return collectionItem;
    }

    public void assertThatItemExists(String itemName, String imageUrl, MyCollection myCollection) {
        Optional.ofNullable(CollectionItem.findByNameAndCollection(itemName, myCollection)).map(item -> {
            if(!item.active) {
                item.active = true;
                item.update();
            }
            return item;
        }).orElseGet(() -> createItem(itemName, imageUrl, myCollection));
        Assert.assertNotNull(CollectionItem.findByNameAndCollection(itemName, myCollection));
        Assert.assertTrue(CollectionItem.findByNameAndCollection(itemName, myCollection).active);
    }
}