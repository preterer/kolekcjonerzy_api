package helpers;

import dto.out.FriendRequestJsonOut;
import dto.out.FriendRequestsJson;
import models.Account;
import models.FriendRequest;
import org.junit.Assert;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by ja on 29.11.15.
 */
public class FriendsHelpers {
    private static FriendRequest createFriendsRequest(Account from, Account to) {
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.message = "Message";
        friendRequest.active = true;
        friendRequest.from = from;
        friendRequest.to = to;
        friendRequest.save();
        return friendRequest;
    }

    public static void assertRequestExists(Account from, Account to) {
        Optional.ofNullable(FriendRequest.findByFromAndTo(from, to)).orElseGet(() -> createFriendsRequest(from, to));
        Assert.assertNotNull(FriendRequest.findByFromAndTo(from, to));
    }

    public static void assertRequestsEqualsDbState(Account account, FriendRequestsJson json) {
        List<FriendRequest> fromList = FriendRequest.findByFromActive(account);
        List<FriendRequest> toList = FriendRequest.findByToActive(account);
        List<UUID> receivedRequestsIds = json.getReceived().stream().map(FriendRequestJsonOut::getId).collect(Collectors.toList());
        List<UUID> sentRequestsIds = json.getSent().stream().map(FriendRequestJsonOut::getId).collect(Collectors.toList());
        toList.stream().map(request -> request.id).forEach(id -> Assert.assertTrue(receivedRequestsIds.contains(id)));
        fromList.stream().map(request -> request.id).forEach(id -> Assert.assertTrue(sentRequestsIds.contains(id)));
    }
}
