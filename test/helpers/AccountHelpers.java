package helpers;

import dto.out.AccountShortJson;
import models.Account;
import org.junit.Assert;

/**
 * Created by ja on 29.11.15.
 */
public class AccountHelpers {
    public static void assertAccountEqualsJson(Account account, AccountShortJson json) {
        Assert.assertEquals(account.email, json.getEmail());
        Assert.assertEquals(account.name, json.getName());
        Assert.assertEquals(account.lastName, json.getLastName());
    }

    public static void assertAccountHasNameAndLastName(Account account, String name, String lastName) {
        Assert.assertEquals(account.name, name);
        Assert.assertEquals(account.lastName, lastName);
    }
}
