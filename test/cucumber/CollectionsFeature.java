package cucumber;

import com.google.common.collect.Lists;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.in.CollectionItemJson;
import dto.in.CollectionJson;
import dto.out.MyCollectionLongJson;
import helpers.CollectionHelpers;
import helpers.GlobalHelpers;
import models.Account;
import models.MyCollection;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import play.libs.Json;
import play.mvc.Http;
import utils.Constants;
import utils.JsonUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class CollectionsFeature {

    @Given("^a token for requests$")
    public void a_token_for_requests() throws Throwable {
        GlobalHelpers.INSTANCE.getToken();
        GlobalHelpers.INSTANCE.assertTokenIsPresent();
    }

    @Given("^a collections list$")
    public void a_collections_list() throws Throwable {
        Assert.assertThat(MyCollection.FINDER.all(), IsNull.notNullValue());
    }

    @Given("^a collection named \"([^\"]*)\"$")
    public void a_collection_named(String collectionName) throws Throwable {
        CollectionHelpers.INSTANCE.assertThatCollectionExists(collectionName);
    }

    @Given("^a collection \"([^\"]*)\" containing item named \"([^\"]*)\" with url \"([^\"]*)\"$")
    public void a_collection_containing_item_named_with_url(String collectionName, String itemName, String imageUrl) throws Throwable {
        CollectionHelpers.INSTANCE.assertThatCollectionExists(collectionName);
        MyCollection myCollection = MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id);
        CollectionHelpers.INSTANCE.assertThatItemExists(itemName, imageUrl, myCollection);
    }

    @Given("^(\\d+) collections$")
    public void collections(int amount) throws Throwable {
        List<MyCollection> collections = MyCollection.findActive();
        if (collections.size() < amount) {
            for (int i = 0; i < collections.size() - amount; i++) {
                CollectionHelpers.INSTANCE.createCollection("Collecion number" + i);
            }
        }
    }

    @When("^I try to get collections list with token$")
    public void I_try_to_get_collections_list_with_token() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/collection"));
    }

    @When("^I try to get collections list without token$")
    public void I_try_to_get_collections_list_without_token() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("GET", "/collection"));
    }

    @When("^I try to get a collection named \"([^\"]*)\" with token$")
    public void I_try_to_get_a_collection_named_with_token(String collectionName) throws Throwable {
        MyCollection myCollection = MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/collection/" + myCollection.id));
    }

    @When("^I try to get a collection named \"([^\"]*)\" without token$")
    public void I_try_to_get_a_collection_named_without_token(String collectionName) throws Throwable {
        MyCollection myCollection = MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id);
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("GET", "/collection/" + myCollection.id));
    }

    @When("^I try to add a collection named \"([^\"]*)\" with token$")
    public void I_try_to_add_a_collection_named_with_token(String collectionName) throws Throwable {
        CollectionJson collectionJson = new CollectionJson();
        collectionJson.setItems(Lists.newArrayList());
        collectionJson.setName(collectionName);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("POST", "/collection").withJsonBody(Json.toJson(collectionJson)));
    }

    @When("^I try to add a collection named \"([^\"]*)\" without token$")
    public void I_try_to_add_a_collection_named_without_token(String collectionName) throws Throwable {
        CollectionJson collectionJson = new CollectionJson();
        collectionJson.setItems(Lists.newArrayList());
        collectionJson.setName(collectionName);
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("POST", "/collection").withJsonBody(Json.toJson(collectionJson)));
    }

    @When("^I try to add a collection named \"([^\"]*)\" with item containing name \"([^\"]*)\" and url \"([^\"]*)\" with token$")
    public void I_try_to_add_a_collection_named_with_item_containing_name_and_url_with_token(String collectionName, String itemName, String imageUrl) throws Throwable {
        CollectionItemJson collectionItemJson = new CollectionItemJson();
        collectionItemJson.setName(itemName);
        collectionItemJson.setImageUrl(imageUrl);
        CollectionJson collectionJson = new CollectionJson();
        collectionJson.setItems(Lists.newArrayList(collectionItemJson));
        collectionJson.setName(collectionName);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("POST", "/collection").withJsonBody(Json.toJson(collectionJson)));
    }

    @When("^I try to add a collection named \"([^\"]*)\" with item containing name \"([^\"]*)\" and url \"([^\"]*)\" without token$")
    public void I_try_to_add_a_collection_named_with_item_containing_name_and_url_without_token(String collectionName, String itemName, String imageUrl) throws Throwable {
        CollectionItemJson collectionItemJson = new CollectionItemJson();
        collectionItemJson.setName(itemName);
        collectionItemJson.setImageUrl(imageUrl);
        CollectionJson collectionJson = new CollectionJson();
        collectionJson.setItems(Lists.newArrayList(collectionItemJson));
        collectionJson.setName(collectionName);
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("POST", "/collection").withJsonBody(Json.toJson(collectionJson)));
    }

    @When("^I try to remove \"([^\"]*)\" with token$")
    public void I_try_to_remove_with_token(String collectionName) throws Throwable {
        UUID collectionId = MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id).id;
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("DELETE", "/collection/" + collectionId.toString()));
    }

    @When("^I try to remove \"([^\"]*)\" without token$")
    public void I_try_to_remove_without_token(String collectionName) throws Throwable {
        UUID collectionId = MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id).id;
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("DELETE", "/collection/" + collectionId.toString()));
    }

    @When("^I try to remove the non-existing collection with token$")
    public void I_try_to_remove_the_non_existing_collection_with_token() throws Throwable {
        UUID collectionId = UUID.randomUUID();
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("DELETE", "/collection/" + collectionId.toString()));
    }

    @When("^I try to remove the non-existing collection without token$")
    public void I_try_to_remove_the_non_existing_collection_without_token() throws Throwable {
        UUID collectionId = UUID.randomUUID();
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("DELETE", "/collection/" + collectionId.toString()));
    }

    @When("^I try to change collections name from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void I_try_to_change_collections_name_from_to(String oldName, String newName) throws Throwable {
        Account account = GlobalHelpers.INSTANCE.getAdminAccount();
        MyCollection collection = MyCollection.findByNameAndUserIdActive(oldName, account.id);
        CollectionJson json = new CollectionJson();
        json.setName(newName);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("PUT", "/collection/" + collection.id).withJsonBody(Json.toJson(json)));
    }

    @When("^I try to get random collections$")
    public void I_try_to_get_random_collections() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("GET", "/collection/random"));
    }

    @Then("^The collection \"([^\"]*)\" is removed$")
    public void The_collection_is_removed(String collectionName) throws Throwable {
        Assert.assertNull(MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id));
    }

    @Then("^The collection \"([^\"]*)\" is not removed$")
    public void The_collection_is_not_removed(String collectionName) throws Throwable {
        Assert.assertNotNull(MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id));
    }

    @Then("^I get the list$")
    public void I_get_the_list() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.OK);
    }

    @Then("^There is a collection \"([^\"]*)\"$")
    public void There_is_a_collection(String collectionName) throws Throwable {
        Account account = GlobalHelpers.INSTANCE.getAdminAccount();
        Assert.assertNotNull(MyCollection.findByNameAndUserIdActive(collectionName, account.id));
    }

    @Then("^There is no collection \"([^\"]*)\"$")
    public void There_is_no_collection(String collectionName) throws Throwable {
        Account account = GlobalHelpers.INSTANCE.getAdminAccount();
        Assert.assertNull(MyCollection.findByNameAndUserIdActive(collectionName, account.id));
    }

    @Then("^I get random collections$")
    public void I_get_random_collections() throws Throwable {
        List response = JsonUtils.getJson(contentAsString(GlobalHelpers.INSTANCE.result), List.class);
        Assert.assertTrue(response.size() == Constants.RANDOM_COLLECTIONS_AMOUNT);
    }

}
