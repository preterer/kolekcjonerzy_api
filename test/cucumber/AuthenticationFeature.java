package cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.in.SignInJson;
import dto.in.SignUpJson;
import helpers.GlobalHelpers;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import play.libs.Json;
import play.mvc.Http;

import static play.test.Helpers.fakeRequest;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class AuthenticationFeature {
    @Given("^an admin account$")
    public void an_admin_account() throws Throwable {
        Assert.assertThat(GlobalHelpers.INSTANCE.getAdminAccount(), IsNull.notNullValue());
    }

    @When("^I sign in with email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void I_sign_in_with_email_and_password(String email, String password) throws Throwable {
        SignInJson signInJson = new SignInJson();
        signInJson.setEmail(email);
        signInJson.setPassword(password);
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("POST", "/signIn").withJsonBody(Json.toJson(signInJson)));
    }

    @Then("^I get success response and auth-token$")
    public void I_get_success_response_and_auth_token() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.NO_CONTENT);
        GlobalHelpers.INSTANCE.assertTokenIsPresent();
    }

    @When("^I sign up with email \"([^\"]*)\", password \"([^\"]*)\", name \"([^\"]*)\" and last name \"([^\"]*)\"$")
    public void I_sign_up_with_email_password_name_and_last_name(String email, String password, String name, String lastName) throws Throwable {
        SignUpJson signUpJson = new SignUpJson();
        signUpJson.setEmail(email);
        signUpJson.setPassword(password);
        signUpJson.setName(name);
        signUpJson.setLastName(lastName);
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("POST", "/signUp").withJsonBody(Json.toJson(signUpJson)));
    }

}
