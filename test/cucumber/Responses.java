package cucumber;

import cucumber.api.java.en.Then;
import helpers.GlobalHelpers;
import play.mvc.Http;

/**
 * Created by ja on 26.11.15.
 */
public class Responses {
    @Then("^I get unauthorized$")
    public void I_get_unauthorized() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.UNAUTHORIZED);
    }

    @Then("^I get success$")
    public void I_get_success() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.NO_CONTENT);
    }

    @Then("^I get forbidden$")
    public void I_get_forbidden() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.FORBIDDEN);
    }

    @Then("^I get noResponse$")
    public void I_get_noResponse() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.NO_CONTENT);
    }

    @Then("^I get notFound$")
    public void I_get_notFound() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.NOT_FOUND);
    }

    @Then("^I get badRequest$")
    public void I_get_badRequest() throws Throwable {
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.BAD_REQUEST);
    }

    @Then("^I get ok$")
    public void I_get_ok() throws Throwable {
        // Express the Regexp above with the code you wish you had
        GlobalHelpers.INSTANCE.assertResponse(Http.Status.OK);
    }

}
