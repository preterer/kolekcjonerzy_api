package cucumber;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.in.ChangePasswordJson;
import dto.in.EditAccountJson;
import dto.out.AccountShortJson;
import helpers.AccountHelpers;
import helpers.GlobalHelpers;
import models.Account;
import org.junit.Assert;
import play.libs.Json;
import security.SecurityUtils;
import utils.JsonUtils;

import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;

/**
 * Created by ja on 29.11.15.
 */
public class AccountFeature {
    @When("^I ask for information about me$")
    public void I_ask_for_information_about_me() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/me"));
    }

    @When("^I ask for information about user \"([^\"]*)\"$")
    public void I_ask_for_information_about_user(String email) throws Throwable {
        Account account = Account.findByEmailActive(email);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/user/" + account.id));
    }

    @When("^I try to change my name to \"([^\"]*)\" and last name to \"([^\"]*)\"$")
    public void I_try_to_change_my_name_to_and_last_name_to(String name, String lastName) throws Throwable {
        EditAccountJson json = new EditAccountJson();
        json.setName(name);
        json.setLastName(lastName);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("PUT", "/me").withJsonBody(Json.toJson(json)));
    }

    @When("^I try to change my password from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void I_try_to_change_my_password_from_to(String oldPassword, String newPassword) throws Throwable {
        ChangePasswordJson json = new ChangePasswordJson();
        json.setOldPassword(oldPassword);
        json.setNewPassword(newPassword);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("PUT", "/me/password").withJsonBody(Json.toJson(json)));
    }

    @When("^I try to disable my account$")
    public void I_try_to_disable_my_account() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("DELETE", "/me/quit"));
    }

    @Then("^I get information about user \"([^\"]*)\"$")
    public void I_get_information_about_user(String email) throws Throwable {
        Account account = Account.findByEmailActive(email);
        AccountShortJson json = JsonUtils.getJson(contentAsString(GlobalHelpers.INSTANCE.result), AccountShortJson.class);
        AccountHelpers.assertAccountEqualsJson(account, json);
    }

    @Then("^User \"([^\"]*)\" has name \"([^\"]*)\" and last name \"([^\"]*)\"$")
    public void User_has_name_and_last_name(String email, String name, String lastName) throws Throwable {
        Account account = Account.findByEmailActive(email);
        AccountHelpers.assertAccountHasNameAndLastName(account, name, lastName);
    }

    @Then("^Password for user \"([^\"]*)\" is set to \"([^\"]*)\"$")
    public void Password_for_user_is_set_to(String email, String password) throws Throwable {
        Account account = Account.findByEmailActive(email);
        SecurityUtils.INSTANCE.verifyPassword(account, password);
    }

    @Then("^The account \"([^\"]*)\" is disabled$")
    public void The_account_is_disabled(String email) throws Throwable {
        Account account = Account.findByEmail(email);
        Assert.assertFalse(account.active);
    }


}
