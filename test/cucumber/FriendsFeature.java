package cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.in.MessageJson;
import dto.out.FriendRequestsJson;
import helpers.FriendsHelpers;
import helpers.GlobalHelpers;
import models.Account;
import models.FriendRequest;
import org.junit.Assert;
import play.libs.Json;
import utils.JsonUtils;

import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;

/**
 * Created by ja on 29.11.15.
 */
public class FriendsFeature {
    @Given("^an invitation from user \"([^\"]*)\" to \"([^\"]*)\"$")
    public void an_invitation_from_user_to(String fromEmail, String toEmail) throws Throwable {
        Account from = Account.findByEmailActive(fromEmail);
        Account to = Account.findByEmailActive(toEmail);
        FriendsHelpers.assertRequestExists(from, to);
    }

    @When("^I invite \"([^\"]*)\"$")
    public void I_invite(String email) throws Throwable {
        Account account = Account.findByEmailActive(email);
        MessageJson json = new MessageJson();
        json.setMessage("Message");
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("POST", "/friends/" + account.id).withJsonBody(Json.toJson(json)));
    }

    @When("^I as \"([^\"]*)\" accept invitation from \"([^\"]*)\"$")
    public void I_as_accept_invitation_from(String to, String from) throws Throwable {
        Account fromAcc = Account.findByEmailActive(from);
        Account toAcc = Account.findByEmailActive(to);
        FriendRequest request = FriendRequest.findByFromAndTo(fromAcc, toAcc);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("POST", "/friends/requests/" + request.id));
    }

    @When("^I as \"([^\"]*)\" reject invitation from \"([^\"]*)\"$")
    public void I_as_reject_invitation_from(String to, String from) throws Throwable {
        Account fromAcc = Account.findByEmailActive(from);
        Account toAcc = Account.findByEmailActive(to);
        FriendRequest request = FriendRequest.findByFromAndTo(fromAcc, toAcc);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("DELETE", "/friends/requests/" + request.id));
    }

    @When("^I ask for my friends requests list$")
    public void I_ask_for_my_friends_requests_list() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/friends/requests"));
    }

    @When("^I ask for my friends requests list with limit (\\d+) and start (\\d+)$")
    public void I_ask_for_my_friends_requests_list_with_limit_and_start(int limit, int start) throws Throwable {
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/friends/requests/limit/" + limit + "/start/" + start));
    }



    @Then("^user \"([^\"]*)\" has invitation to \"([^\"]*)\"$")
    public void user_has_invitation_to(String from, String to) throws Throwable {
        Account fromAcc = Account.findByEmailActive(from);
        Account toAcc = Account.findByEmailActive(to);
        Assert.assertNotNull(FriendRequest.findByFromAndTo(fromAcc, toAcc));
    }

    @Then("^user \"([^\"]*)\" has invitation from \"([^\"]*)\"$")
    public void user_has_invitation_from(String to, String from) throws Throwable {
        Account fromAcc = Account.findByEmailActive(from);
        Account toAcc = Account.findByEmailActive(to);
        Assert.assertNotNull(FriendRequest.findByFromAndTo(fromAcc, toAcc));
    }

    @Then("^user \"([^\"]*)\" has user \"([^\"]*)\" in his friends list$")
    public void user_has_user_in_his_friends_list(String email1, String email2) throws Throwable {
        Assert.assertTrue(Account.findByEmailActive(email1).friends.friends.contains(Account.findByEmailActive(email2)));
    }

    @Then("^user \"([^\"]*)\" doesn't have user \"([^\"]*)\" in his friends list$")
    public void user_doesn_t_have_user_in_his_friends_list(String email1, String email2) throws Throwable {
        Assert.assertFalse(Account.findByEmailActive(email1).friends.friends.contains(Account.findByEmailActive(email2)));
    }

    @Then("^I get list of requests for \"([^\"]*)\"$")
    public void I_get_list_of_requests_for(String email) throws Throwable {
        Account account = Account.findByEmailActive(email);
        FriendRequestsJson json = JsonUtils.getJson(contentAsString(GlobalHelpers.INSTANCE.result), FriendRequestsJson.class);
        FriendsHelpers.assertRequestsEqualsDbState(account, json);
    }


}
