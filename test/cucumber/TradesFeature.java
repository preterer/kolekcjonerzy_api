package cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.in.TradeJson;
import helpers.AuthHelpers;
import helpers.CollectionHelpers;
import helpers.GlobalHelpers;
import helpers.TradesHelper;
import jdk.nashorn.internal.objects.Global;
import models.Account;
import models.CollectionItem;
import models.MyCollection;
import models.Trade;
import org.junit.Assert;
import play.libs.Json;
import utils.JsonUtils;

import java.util.List;

import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;

/**
 * Created by ja on 26.11.15.
 */
public class TradesFeature {
    @Given("^an user \"([^\"]*)\" \"([^\"]*)\" with email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void an_user_with_email_and_password(String name, String lastName, String email, String password) throws Throwable {
        AuthHelpers.INSTANCE.assertAccountExists(email, password, name, lastName);
    }

    @Given("^a token which belongs to user with email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void a_token_which_belongs_to_user_with_email_and_password(String email, String password) throws Throwable {
        GlobalHelpers.INSTANCE.getToken(email, password);
    }

    @Given("^a collection \"([^\"]*)\" containing item named \"([^\"]*)\" with url \"([^\"]*)\" which belongs to user \"([^\"]*)\"$")
    public void a_collection_containing_item_named_with_url_which_belongs_to_user(String collectionName, String itemName, String itemUrl, String email) throws Throwable {
        CollectionHelpers.INSTANCE.assertThatCollectionExistsForUser(collectionName, email);
        CollectionHelpers.INSTANCE.assertThatItemExists(itemName, itemUrl, MyCollection.findByNameAndEmailActive(collectionName, email));
    }

    @Given("^a trade from user \"([^\"]*)\" offering item \"([^\"]*)\" from collection \"([^\"]*)\" for item \"([^\"]*)\" from collection \"([^\"]*)\" owned by user \"([^\"]*)\"$")
    public void a_trade_from_user_offering_item_from_collection_for_item_from_collection_owned_by_user
            (String email1, String item1Name, String collection1Name, String item2Name, String collection2Name, String email2) throws Throwable {
        MyCollection offerCollection = MyCollection.findByNameAndEmailActive(collection1Name, email1);
        MyCollection wantCollection = MyCollection.findByNameAndEmailActive(collection2Name, email2);
        CollectionItem offeredItem = CollectionItem.findByNameAndCollection(item1Name, offerCollection);
        CollectionItem wantedItem = CollectionItem.findByNameAndCollection(item2Name, wantCollection);
        TradesHelper.INSTANCE.assertTradeExists(offeredItem, wantedItem);
    }

    @When("^I offer a trade of item \"([^\"]*)\" belonging to \"([^\"]*)\" and user \"([^\"]*)\" for item \"([^\"]*)\" belonging to \"([^\"]*)\" and user \"([^\"]*)\"$")
    public void I_offer_a_trade_of_item_belonging_to_and_user_for_item_belonging_to_and_user(String item1Name, String col1Name, String email1, String item2Name, String col2Name, String email2) throws Throwable {
        CollectionItem item1 = CollectionItem.findByNameAndCollection(item1Name, MyCollection.findByNameAndEmailActive(col1Name, email1));
        CollectionItem item2 = CollectionItem.findByNameAndCollection(item2Name, MyCollection.findByNameAndEmailActive(col2Name, email2));
        TradeJson json = new TradeJson("message", item1.id, item2.id);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("POST", "/trade").withJsonBody(Json.toJson(json)));
    }

    @When("^I try to accept the trade between items \"([^\"]*)\" from collection \"([^\"]*)\" belonging to user \"([^\"]*)\" and item \"([^\"]*)\" from collection \"([^\"]*)\" belonging to user \"([^\"]*)\"$")
    public void I_try_to_accept_the_trade_between_items_from_collection_belonging_to_user_and_item_from_collection_belonging_to_user
            (String item1Name, String collection1Name, String email1, String item2Name, String collection2Name, String email2) throws Throwable {
        CollectionItem item1 = CollectionItem.findByNameAndCollection(item1Name, MyCollection.findByNameAndEmailActive(collection1Name, email1));
        CollectionItem item2 = CollectionItem.findByNameAndCollection(item2Name, MyCollection.findByNameAndEmailActive(collection2Name, email2));
        Trade trade = Trade.findByTradeItemsActive(item1.id, item2.id);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("POST", "/trade/" + trade.id));
    }

    @When("^I try to reject the trade between items \"([^\"]*)\" from collection \"([^\"]*)\" belonging to user \"([^\"]*)\" and item \"([^\"]*)\" from collection \"([^\"]*)\" belonging to user \"([^\"]*)\"$")
    public void I_try_to_reject_the_trade_between_items_from_collection_belonging_to_user_and_item_from_collection_belonging_to_user    (String item1Name, String collection1Name, String email1, String item2Name, String collection2Name, String email2) throws Throwable {
        CollectionItem item1 = CollectionItem.findByNameAndCollection(item1Name, MyCollection.findByNameAndEmailActive(collection1Name, email1));
        CollectionItem item2 = CollectionItem.findByNameAndCollection(item2Name, MyCollection.findByNameAndEmailActive(collection2Name, email2));
        Trade trade = Trade.findByTradeItemsActive(item1.id, item2.id);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("DELETE", "/trade/" + trade.id));
    }

    @When("^I try to get my offers$")
    public void I_try_to_get_my_offers() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/trade/offers"));
    }

    @When("^I try to get my requests$")
    public void I_try_to_get_my_requests() throws Throwable {
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("GET", "/trade/requests"));
    }

    @Then("^Other trades of item \"([^\"]*)\" from collection \"([^\"]*)\" belonging to user \"([^\"]*)\" are gone$")
    public void Other_trades_of_item_from_collection_belonging_to_user_are_gone(String itemName, String collectionName, String email) throws Throwable {
        CollectionItem item = CollectionItem.findByNameAndCollection(itemName, MyCollection.findByNameAndEmailActive(collectionName, email));
        List<Trade> trades = Trade.findActiveByItemId(item.id);
        Assert.assertTrue(trades.isEmpty());
    }

    @Then("^Other trades of item \"([^\"]*)\" from collection \"([^\"]*)\" belonging to user \"([^\"]*)\" are remaining$")
    public void Other_trades_of_item_from_collection_belonging_to_user_are_remaining(String itemName, String collectionName, String email) throws Throwable {
        CollectionItem item = CollectionItem.findByNameAndCollection(itemName, MyCollection.findByNameAndEmailActive(collectionName, email));
        List<Trade> trades = Trade.findActiveByItemId(item.id);
        Assert.assertFalse(trades.isEmpty());
    }

    @Then("^I get some trades$")
    public void I_get_some_trades() throws Throwable {
        List response = JsonUtils.getJson(contentAsString(GlobalHelpers.INSTANCE.result), List.class);
        Assert.assertFalse(response.isEmpty());
    }


}
