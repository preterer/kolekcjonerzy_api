package cucumber;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.in.CollectionItemJson;
import helpers.GlobalHelpers;
import models.Account;
import models.CollectionItem;
import models.MyCollection;
import org.junit.Assert;
import play.libs.Json;

import java.util.Optional;
import java.util.UUID;

import static play.test.Helpers.fakeRequest;

/**
 * Created by ja on 29.11.15.
 */
public class ItemsFeature {
    @When("^I try to add an item to the collection named \"([^\"]*)\" with token$")
    public void I_try_to_add_an_item_to_the_collection_named_with_token(String collectionName) throws Throwable {
        UUID collectionId = Optional.ofNullable(MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id))
                .map(collection -> collection.id)
                .orElse(UUID.randomUUID());
        CollectionItemJson collectionItemJson = new CollectionItemJson();
        collectionItemJson.setImageUrl("test");
        collectionItemJson.setName("test");
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("POST", "/collection/" + collectionId.toString() +
                "/item").withJsonBody(Json.toJson(collectionItemJson)));
    }

    @When("^I try to add an item to the collection named \"([^\"]*)\" without token$")
    public void I_try_to_add_an_item_to_the_collection_named_without_token(String collectionName) throws Throwable {
        UUID collectionId = Optional.ofNullable(MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id))
                .map(collection -> collection.id)
                .orElse(UUID.randomUUID());
        CollectionItemJson collectionItemJson = new CollectionItemJson();
        collectionItemJson.setImageUrl("test");
        collectionItemJson.setName("test");
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("POST", "/collection/" + collectionId.toString() +
                "/item").withJsonBody(Json.toJson(collectionItemJson)));
    }

    @When("^I try to remove the item \"([^\"]*)\" from \"([^\"]*)\" with token$")
    public void I_try_to_remove_the_item_from_with_token(String itemName, String collectionName) throws Throwable {
        UUID itemId = CollectionItem.findByNameAndCollection(itemName, MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id)).id;
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("DELETE", "/item/" + itemId.toString()));
    }

    @When("^I try to remove the item \"([^\"]*)\" from \"([^\"]*)\" without token$")
    public void I_try_to_remove_the_item_from_without_token(String itemName, String collectionName) throws Throwable {
        UUID itemId = CollectionItem.findByNameAndCollection(itemName, MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id)).id;
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("DELETE", "/item/" + itemId.toString()));
    }

    @When("^I try to remove an item which doesn't exist with token$")
    public void I_try_to_remove_an_item_which_doesn_t_exist_with_token() throws Throwable {
        UUID itemId = UUID.randomUUID();
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("DELETE", "/item/" + itemId.toString()));
    }

    @When("^I try to remove an item which doesn't exist without token$")
    public void I_try_to_remove_an_item_which_doesn_t_exist_without_token() throws Throwable {
        UUID itemId = UUID.randomUUID();
        GlobalHelpers.INSTANCE.routeWithoutToken(fakeRequest("DELETE", "/item/" + itemId.toString()));
    }

    @When("^I try to move item \"([^\"]*)\" from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void I_try_to_move_item_from_to(String itemName, String coll1Name, String coll2Name) throws Throwable {
        Account account = GlobalHelpers.INSTANCE.getAdminAccount();
        MyCollection ownerCollection = MyCollection.findByNameAndUserIdActive(coll1Name, account.id);
        MyCollection nextCollection = MyCollection.findByNameAndUserIdActive(coll2Name, account.id);
        CollectionItem item = CollectionItem.findByNameAndCollection(itemName, ownerCollection);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("PUT", "/item/" + item.id + "/move/" + nextCollection.id));
    }

    @When("^I try to change items \"([^\"]*)\" from collection \"([^\"]*)\" name to \"([^\"]*)\" and link to \"([^\"]*)\"$")
    public void I_try_to_change_items_from_collection_name_to_and_link_to(String itemName, String collectionName, String newName, String newLink) throws Throwable {
        Account account = GlobalHelpers.INSTANCE.getAdminAccount();
        MyCollection collection = MyCollection.findByNameAndUserIdActive(collectionName, account.id);
        CollectionItem item = CollectionItem.findByNameAndCollection(itemName, collection);
        CollectionItemJson json = new CollectionItemJson();
        json.setName(newName);
        json.setImageUrl(newLink);
        GlobalHelpers.INSTANCE.routeWithToken(fakeRequest("PUT", "/item/" + item.id).withJsonBody(Json.toJson(json)));
    }

    @Then("^The item \"([^\"]*)\" is not removed from \"([^\"]*)\"$")
    public void The_item_is_not_removed_from(String itemName, String collectionName) throws Throwable {
        Assert.assertTrue(CollectionItem.findByNameAndCollection(itemName, MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id)).active);
    }

    @Then("^The item \"([^\"]*)\" is removed from \"([^\"]*)\"$")
    public void The_item_is_removed_from(String itemName, String collectionName) throws Throwable {
        Assert.assertFalse(CollectionItem.findByNameAndCollection(itemName, MyCollection.findByNameAndUserIdActive(collectionName, GlobalHelpers.INSTANCE.getAdminAccount().id)).active);
    }

    @Then("^Item \"([^\"]*)\" is in collection \"([^\"]*)\"$")
    public void Item_is_in_collection(String itemName, String collectionName) throws Throwable {
        Account account = GlobalHelpers.INSTANCE.getAdminAccount();
        MyCollection collection = MyCollection.findByNameAndUserIdActive(collectionName, account.id);
        Assert.assertNotNull(CollectionItem.findByNameAndCollection(itemName, collection));
    }

    @Then("^Item \"([^\"]*)\" is not in collection \"([^\"]*)\"$")
    public void Item_is_not_in_collection(String itemName, String collectionName) throws Throwable {
        Account account = GlobalHelpers.INSTANCE.getAdminAccount();
        MyCollection collection = MyCollection.findByNameAndUserIdActive(collectionName, account.id);
        Assert.assertNull(CollectionItem.findByNameAndCollection(itemName, collection));
    }
}
