/**
 * Created by kchudy on 3/15/15.
 */

import cucumber.api.java.Before;
import play.test.FakeApplication;
import play.test.TestBrowser;
import play.test.TestServer;

import static play.test.Helpers.*;

public class GlobalHooks {
    public static int PORT = 3333;
    public static TestBrowser TEST_BROWSER;
    public static TestServer TEST_SERVER;
    public static FakeApplication FAKE_APPLICATION;
    public static boolean initialised = false;

    @Before
    public static void before() {
        if (!initialised) {
            FAKE_APPLICATION = fakeApplication(inMemoryDatabase());
            TEST_SERVER = testServer(PORT, FAKE_APPLICATION);
            TEST_BROWSER = testBrowser(HTMLUNIT, PORT);
            start(TEST_SERVER);
            initialised = true;
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    TEST_BROWSER.quit();
                    TEST_SERVER.stop();
                    initialised = false;
                }
            });
        }
    }
}