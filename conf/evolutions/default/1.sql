# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table account (
  id                        varchar(40) not null,
  creation_date             timestamp not null,
  update_date               timestamp not null,
  active                    boolean not null,
  email                     varchar(255) not null,
  password                  varchar(255) not null,
  name                      varchar(255) not null,
  last_name                 varchar(255) not null,
  constraint uq_account_email unique (email),
  constraint pk_account primary key (id))
;

create table collection_item (
  id                        varchar(40) not null,
  creation_date             timestamp not null,
  update_date               timestamp not null,
  active                    boolean not null,
  name                      varchar(255) not null,
  image_url                 varchar(255),
  description               varchar(255),
  my_collection_id          varchar(40),
  account_id                varchar(40),
  constraint pk_collection_item primary key (id))
;

create table friend_request (
  id                        varchar(40) not null,
  creation_date             timestamp not null,
  update_date               timestamp not null,
  active                    boolean not null,
  from_id                   varchar(40),
  to_id                     varchar(40),
  message                   varchar(255),
  constraint pk_friend_request primary key (id))
;

create table friends_list (
  id                        varchar(40) not null,
  creation_date             timestamp not null,
  update_date               timestamp not null,
  active                    boolean not null,
  account_id                varchar(40),
  constraint pk_friends_list primary key (id))
;

create table my_collection (
  id                        varchar(40) not null,
  creation_date             timestamp not null,
  update_date               timestamp not null,
  active                    boolean not null,
  name                      varchar(255) not null,
  description               varchar(255),
  account_id                varchar(40),
  constraint pk_my_collection primary key (id))
;

create table role (
  id                        varchar(40) not null,
  creation_date             timestamp not null,
  update_date               timestamp not null,
  active                    boolean not null,
  name                      varchar(255),
  constraint uq_role_name unique (name),
  constraint pk_role primary key (id))
;

create table trade (
  id                        varchar(40) not null,
  creation_date             timestamp not null,
  update_date               timestamp not null,
  active                    boolean not null,
  offered_item_id           varchar(40),
  wanted_item_id            varchar(40),
  message                   varchar(64000),
  constraint pk_trade primary key (id))
;


create table account_role (
  account_id                     varchar(40) not null,
  role_id                        varchar(40) not null,
  constraint pk_account_role primary key (account_id, role_id))
;

create table friends_list_account (
  friends_list_id                varchar(40) not null,
  account_id                     varchar(40) not null,
  constraint pk_friends_list_account primary key (friends_list_id, account_id))
;
alter table collection_item add constraint fk_collection_item_myCollectio_1 foreign key (my_collection_id) references my_collection (id) on delete restrict on update restrict;
create index ix_collection_item_myCollectio_1 on collection_item (my_collection_id);
alter table collection_item add constraint fk_collection_item_account_2 foreign key (account_id) references account (id) on delete restrict on update restrict;
create index ix_collection_item_account_2 on collection_item (account_id);
alter table friend_request add constraint fk_friend_request_from_3 foreign key (from_id) references account (id) on delete restrict on update restrict;
create index ix_friend_request_from_3 on friend_request (from_id);
alter table friend_request add constraint fk_friend_request_to_4 foreign key (to_id) references account (id) on delete restrict on update restrict;
create index ix_friend_request_to_4 on friend_request (to_id);
alter table friends_list add constraint fk_friends_list_account_5 foreign key (account_id) references account (id) on delete restrict on update restrict;
create index ix_friends_list_account_5 on friends_list (account_id);
alter table my_collection add constraint fk_my_collection_account_6 foreign key (account_id) references account (id) on delete restrict on update restrict;
create index ix_my_collection_account_6 on my_collection (account_id);
alter table trade add constraint fk_trade_offeredItem_7 foreign key (offered_item_id) references collection_item (id) on delete restrict on update restrict;
create index ix_trade_offeredItem_7 on trade (offered_item_id);
alter table trade add constraint fk_trade_wantedItem_8 foreign key (wanted_item_id) references collection_item (id) on delete restrict on update restrict;
create index ix_trade_wantedItem_8 on trade (wanted_item_id);



alter table account_role add constraint fk_account_role_account_01 foreign key (account_id) references account (id) on delete restrict on update restrict;

alter table account_role add constraint fk_account_role_role_02 foreign key (role_id) references role (id) on delete restrict on update restrict;

alter table friends_list_account add constraint fk_friends_list_account_frien_01 foreign key (friends_list_id) references friends_list (id) on delete restrict on update restrict;

alter table friends_list_account add constraint fk_friends_list_account_accou_02 foreign key (account_id) references account (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists account;

drop table if exists account_role;

drop table if exists collection_item;

drop table if exists friend_request;

drop table if exists friends_list;

drop table if exists friends_list_account;

drop table if exists my_collection;

drop table if exists role;

drop table if exists trade;

SET REFERENTIAL_INTEGRITY TRUE;

