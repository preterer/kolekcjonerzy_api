package models;

import org.joda.time.DateTime;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */

@MappedSuperclass
public class BaseModel extends Model {
    @Id
    @Column(updatable = false, nullable = false)
    public UUID id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false, nullable = false)
    public Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = true, nullable = false)
    public Date updateDate;

    @Column(nullable = false)
    public Boolean active = true;

    @Override
    public void save() {
        creationDate = updateDate = DateTime.now().toDate();
        id = UUID.randomUUID();
        super.save();
    }

    @Override
    public void update() {
        updateDate = new Date();
        super.update();
    }

    public void saveOrUpdate() {
        if (id == null) save();
        else update();
    }
}
