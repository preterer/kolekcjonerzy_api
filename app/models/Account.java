package models;

import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;
import com.avaje.ebean.Expr;
import com.google.common.collect.Lists;
import dto.in.SignUpJson;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */
@Entity
public class Account extends BaseModel implements Subject {
    public static final Finder<UUID, Account> FINDER = new Finder<>(UUID.class, Account.class);
    @Column(nullable = false, unique = true)
    public String email;
    @Column(nullable = false)
    public String password;
    @Column(nullable = false)
    public String name;
    @Column(nullable = false)
    public String lastName;
    @ManyToMany
    public List<models.Role> roles = Lists.newArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    public List<MyCollection> collections = Lists.newArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    public List<CollectionItem> items = Lists.newArrayList();

    @OneToOne(mappedBy = "account")
    @Column(nullable = false)
    public FriendsList friends;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "to")
    public List<FriendRequest> friendRequests = Lists.newArrayList();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "from")
    public List<FriendRequest> sentFriendRequests = Lists.newArrayList();

    //FINDERS

    public static Account findByEmail(String email) {
        return FINDER.where().eq("email", email).findUnique();
    }

    public static Account findByEmailActive(String email) {
        return FINDER.where().eq("email", email).eq("active", true).findUnique();
    }

    public static Account findByLoginDataActive(String email, String password) {
        return FINDER.where().eq("email", email).eq("password", password).eq("active", true).findUnique();
    }

    public static List<Account> findByNamesAndLastNames(String query) {
        return FINDER.where().or(Expr.like("name", "%" + query + "%"), Expr.ilike("lastName", "%" + query + "%")).eq("active", true).findList();
    }

    public static List<Account> findByNamesAndLastNamesLimited(String query, Integer limit, Integer start) {
        return FINDER.where().or(Expr.like("name", "%" + query + "%"), Expr.ilike("lastName", "%" + query + "%")).eq("active", true)
                .setMaxRows(limit).setFirstRow(start).findList();
    }

    public static Account findByIdActive(UUID id) {
        return FINDER.where().eq("id", id).eq("active", true).findUnique();
    }

    /***
     *
     * @param json sent by user
     * @return newly created Account based on the json, with a generated token
     */

    public static Account getNewAccount(SignUpJson json) {
        Account account = new Account();
        account.email = json.getEmail();
        account.password = json.getPassword();
        account.name = json.getName();
        account.lastName = json.getLastName();
        account.roles.add(models.Role.findUserRole());
        account.save();
        return account;
    }

    //ENDOF FINDERS

    //DEADBOLT

    @Override
    public List<? extends Role> getRoles() {
        return roles;
    }

    @Override
    public List<? extends Permission> getPermissions() {
        return Lists.newArrayList();
    }

    @Override
    public String getIdentifier() {
        return id.toString();
    }

    //ENDOF DEADBOLT

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Account account = (Account) o;

        return id.equals(account.id);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }

    @Override
    public void save() {
        super.save();
        FriendsList.createEmptyFriendsList(this);
    }
}
