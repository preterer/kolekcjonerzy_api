package models;

import com.google.common.collect.Lists;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */
@Entity
public class FriendsList extends BaseModel {
    public static final Finder<UUID, FriendsList> FINDER = new Finder<>(UUID.class, FriendsList.class);

    @OneToOne
    public Account account;

    @ManyToMany
    public List<Account> friends = Lists.newArrayList();

    public static void createEmptyFriendsList(Account account) {
        FriendsList friendsList = new FriendsList();
        friendsList.account = account;
        friendsList.save();
    }

    public static FriendsList findByOwnerAndFriend(Account owner, Account friend) {
        return findByOwnerAndFriend(owner.id, friend.id);
    }

    public static FriendsList findByOwnerAndFriend(UUID ownerId, UUID friendId) {
        return FINDER.where().eq("account.id", ownerId).eq("friends.id", friendId).findUnique();
    }
}
