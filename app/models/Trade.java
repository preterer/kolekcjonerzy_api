package models;

import com.avaje.ebean.Expr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.UUID;

/**
 * Created by mbaranowski on 03.06.15.
 */

@Entity
public class Trade extends BaseModel {
    public static final Finder<UUID, Trade> FINDER = new Finder<UUID, Trade>(UUID.class, Trade.class);
    @OneToOne
    public CollectionItem offeredItem;
    @OneToOne
    public CollectionItem wantedItem;
    @Column(length = 64000)
    public String message;

    public static Trade findById(String id){
        return FINDER.byId(UUID.fromString(id));
    }

    public static Trade findByIdAndWantedItemUserIdActive (UUID id, UUID userId) {
        return FINDER.where().eq("id", id).eq("wantedItem.account.id", userId).eq("active", true).findUnique();
    }

    public static List<Trade> findOffersByAccountActive(UUID accountId) {
        return FINDER.where().eq("wantedItem.account.id", accountId).eq("active", true).findList();
    }

    public static List<Trade> findRequestsByAccountActive(UUID accountId) {
        return FINDER.where().eq("offeredItem.account.id", accountId).eq("active", true).findList();
    }

    public static List<Trade> findActiveByItemId(UUID itemId) {
        return FINDER.where().or(Expr.eq("wantedItem.id", itemId), Expr.eq("offeredItem.id", itemId)).eq("active", true).findList();
    }

    public static Trade findByTradeItemsActive(UUID id1, UUID id2) {
        return FINDER.where().or(
                Expr.and(Expr.eq("wantedItem.id", id1), Expr.eq("offeredItem.id", id2)),
                Expr.and(Expr.eq("wantedItem.id", id2), Expr.eq("offeredItem.id", id1))
        ).eq("active", true).findUnique();
    }

    @Override
    public String toString() {
        return "Trade{" +
                "offeredItem=" + offeredItem +
                ", wantedItem=" + wantedItem +
                ", message='" + message + '\'' +
                ", active=" + active +
                '}';
    }
}
