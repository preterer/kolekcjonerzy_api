package models;

import com.avaje.ebean.Expr;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.List;
import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */
@Entity
public class FriendRequest extends BaseModel {
    public static final Finder<UUID, FriendRequest> FINDER = new Finder<>(UUID.class, FriendRequest.class);

    @ManyToOne
    public Account from;
    @ManyToOne
    public Account to;
    public String message;

    public static FriendRequest findByIdActive(UUID id) {
        return FINDER.where().eq("id", id).eq("active", true).findUnique();
    }

    public static FriendRequest findByIdAndUserToActive(UUID id, UUID userId) {
        return FINDER.where().eq("id", id).eq("to.id", userId).eq("active", true).findUnique();
    }

    public static FriendRequest findByFromAndTo(Account from, Account to) {
        return findByFromAndTo(from.id, to.id);
    }

    public static FriendRequest findByFromAndTo(UUID from, UUID to) {
        return FINDER.where().eq("to.id", to).eq("from.id", from).findUnique();
    }

    public static FriendRequest findByFromOrTo(Account account1, Account account2) {
        return findByFromOrTo(account1.id, account2.id);
    }

    public static FriendRequest findByFromOrTo(UUID id1, UUID id2) {
        return FINDER.where().or(
                Expr.and(Expr.eq("to.id", id2), Expr.eq("from.id", id1)),
                Expr.and(Expr.eq("to.id", id1), Expr.eq("from.id", id2))
        ).findUnique();
    }

    public static List<FriendRequest> findByToActive(Account account) {
        return findByToActive(account.id);
    }

    public static List<FriendRequest> findByToActive(UUID to) {
        return FINDER.where().eq("to.id", to).eq("active", true).findList();
    }

    public static List<FriendRequest> findByFromActive(Account account) {
        return findByFromActive(account.id);
    }

    public static List<FriendRequest> findByFromActive(UUID from) {
        return FINDER.where().eq("from.id", from).eq("active", true).findList();
    }

    public static List<FriendRequest> findByToWithLimitActive(Account account, Integer limit, Integer start) {
        return findByToWithLimitActive(account.id, limit, start);
    }

    public static List<FriendRequest> findByToWithLimitActive(UUID to, Integer limit, Integer start) {
        return FINDER.where().eq("to.id", to).eq("active", true).setMaxRows(limit).setFirstRow(start).findList();
    }

    public static List<FriendRequest> findByFromWithLimitActive(Account account, Integer limit, Integer start) {
        return findByFromWithLimitActive(account.id, limit, start);
    }

    public static List<FriendRequest> findByFromWithLimitActive(UUID from, Integer limit, Integer start) {
        return FINDER.where().eq("from.id", from).eq("active", true).setMaxRows(limit).setFirstRow(start).findList();
    }
}
