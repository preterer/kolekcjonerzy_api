package models;

import utils.Constants;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */
@Entity
public class Role extends BaseModel implements be.objectify.deadbolt.core.models.Role {
    public static final Finder<UUID, Role> FINDER = new Finder<>(UUID.class, Role.class);

    @Column(unique = true)
    public String name;

    @Override
    public String getName() {
        return name;
    }

    public static Role findByName(String name) {
        return FINDER.where().eq("name", name).findUnique();
    }

    public static Role findUserRole() {
        return findByName(Constants.USER_ROLE);
    }

    public static Role findAdminRole() {
        return findByName(Constants.ADMIN_ROLE);
    }
}
