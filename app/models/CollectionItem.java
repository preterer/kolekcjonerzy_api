package models;

import com.avaje.ebean.Expr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.UUID;

/**
 * Created by mbaranowski on 01.06.15.
 */

@Entity
public class CollectionItem extends BaseModel {
    public static final Finder<UUID, CollectionItem> FINDER = new Finder<>(UUID.class, CollectionItem.class);
    @Column(nullable = false)
    public String name;
    public String imageUrl;
    @Column(nullable = true)
    public String description;

    @ManyToOne
    @Column(nullable = false)
    public MyCollection myCollection;

    @ManyToOne
    @Column(nullable = false)
    public Account account;

    public static CollectionItem findByIdNotOwnedByUserActive(UUID id, UUID userId) {
        return FINDER.where().eq("id", id).not(Expr.eq("account.id", userId)).eq("active", true).findUnique();
    }

    public static CollectionItem findByIdAndUserIdActive (UUID id, UUID userId) {
        return FINDER.where().eq("id", id).eq("account.id", userId).eq("active", true).findUnique();
    }

    public static CollectionItem findById(String id){
        return FINDER.byId(UUID.fromString(id));
    }

    public static CollectionItem findByNameAndCollection(String name, MyCollection myCollection){
        return FINDER.where().eq("name", name).eq("myCollection.id", myCollection.id).findUnique();
    }

    @Override
    public String toString() {
        return "CollectionItem{" +
                "id = '" + id + '\'' +
                "name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
