package models;

import utils.Constants;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by mbaranowski on 01.06.15.
 */

@Entity
public class MyCollection extends BaseModel {
    public static final Finder<UUID, MyCollection> FINDER = new Finder<>(UUID.class, MyCollection.class);
    @Column(nullable = false)
    public String name;

    @Column(nullable = true)
    public String description;

    @ManyToOne
    @Column(nullable = false)
    public Account account;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "myCollection")
    public List<CollectionItem> collectionItems;

    public static MyCollection findByNameAndUserIdActive(String name, UUID userId) {
        return FINDER.where().eq("name", name).eq("account.id", userId).eq("active", true).findUnique();
    }

    public static MyCollection findByNameAndEmailActive(String name, String email) {
        return FINDER.where().eq("name", name).eq("account.email", email).eq("active", true).findUnique();
    }

    public static MyCollection findActiveById(String id) {
        return FINDER.where().eq("id", UUID.fromString(id)).eq("active", true).findUnique();
    }

    public static MyCollection findByIdAndUserIdActive(UUID collectionId, UUID userId) {
        return FINDER.where().eq("id", collectionId).eq("account.id", userId).eq("active", true).findUnique();
    }

    public static List<MyCollection> findActiveByUser(Account account) {
        return findActiveByUserId(account.id);
    }

    public static List<MyCollection> findActiveByUserId(UUID userId) {
        return FINDER.where().eq("account.id", userId).eq("active", true).findList();
    }

    public static List<MyCollection> findByNameLike(String query) {
        return FINDER.where().ilike("name", "%" + query + "%").eq("active", true).findList();
    }

    public static List<MyCollection> findByNameLikeLimited(String query, Integer limit, Integer start) {
        return FINDER.where().ilike("name", "%" + query + "%").eq("active", true).setMaxRows(limit).setFirstRow(start).findList();
    }

    public static List<MyCollection> findActive() {
        return FINDER.where().eq("active", true).findList();
    }

    public static List<MyCollection> findRandom() {
        return FINDER.where().eq("active", true).setOrderBy("RAND()").setMaxRows(Constants.RANDOM_COLLECTIONS_AMOUNT).findList();
    }

    @Override
    public String toString() {
        return "MyCollection{" +
                "name='" + name + '\'' +
                ", account=" + account +
                ", collectionItems=" + collectionItems +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        MyCollection that = (MyCollection) o;

        return !(id != null ? !id.equals(that.id) : that.id!= null);

    }
}
