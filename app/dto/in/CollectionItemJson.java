package dto.in;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by mbaranowski on 01.06.15.
 */
public class CollectionItemJson {
    private String name;
    private String imageUrl;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
