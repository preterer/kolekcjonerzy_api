package dto.in;

import java.util.UUID;

/**
 * Created by mbaranowski on 03.06.15.
 */
public class TradeJson {
    private String message;
    private UUID offeredItemId;
    private UUID wantedItemId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UUID getOfferedItemId() {
        return offeredItemId;
    }

    public void setOfferedItemId(UUID offeredItemId) {
        this.offeredItemId = offeredItemId;
    }

    public UUID getWantedItemId() {
        return wantedItemId;
    }

    public void setWantedItemId(UUID wantedItemId) {
        this.wantedItemId = wantedItemId;
    }

    public TradeJson(String message, UUID offeredItemId, UUID wantedItemId) {
        this.message = message;
        this.offeredItemId = offeredItemId;
        this.wantedItemId = wantedItemId;
    }

    public TradeJson() {
        super();
    }
}
