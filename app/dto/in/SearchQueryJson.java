package dto.in;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by ja on 29.11.15.
 */
public class SearchQueryJson {
    private String query;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Integer limit;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Integer start;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }
}
