package dto.in;

/**
 * Created by ja on 29.11.15.
 */
public class MessageJson {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
