package dto.in;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * Created by ja on 29.11.15.
 */
@ApiModel("EditAccountJson")
public class EditAccountJson {
    private String name;
    private String lastName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
