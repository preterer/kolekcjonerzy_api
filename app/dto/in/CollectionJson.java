package dto.in;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by mbaranowski on 01.06.15.
 */
public class CollectionJson {
    private String name;
    private List<CollectionItemJson> items;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CollectionItemJson> getItems() {
        return items;
    }

    public void setItems(List<CollectionItemJson> items) {
        this.items = items;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
