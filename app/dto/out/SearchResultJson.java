package dto.out;

import java.util.List;

/**
 * Created by ja on 29.11.15.
 */
public class SearchResultJson {
    private List<AccountShortJson> accounts;
    private List<MyCollectionShortJson> collections;

    public List<AccountShortJson> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountShortJson> accounts) {
        this.accounts = accounts;
    }

    public List<MyCollectionShortJson> getCollections() {
        return collections;
    }

    public void setCollections(List<MyCollectionShortJson> collections) {
        this.collections = collections;
    }
}
