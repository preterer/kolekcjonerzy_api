package dto.out;

import java.util.UUID;

/**
 * Created by mbaranowski on 11.06.15.
 */
public class TradeJsonOut {
    private UUID id;
    private CollectionItemOutJson offeredItem;
    private CollectionItemOutJson wantedItem;
    private String message;

    public CollectionItemOutJson getOfferedItem() {
        return offeredItem;
    }

    public void setOfferedItem(CollectionItemOutJson offeredItem) {
        this.offeredItem = offeredItem;
    }

    public CollectionItemOutJson getWantedItem() {
        return wantedItem;
    }

    public void setWantedItem(CollectionItemOutJson wantedItem) {
        this.wantedItem = wantedItem;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
