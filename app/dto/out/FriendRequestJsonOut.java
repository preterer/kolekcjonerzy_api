package dto.out;

import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */
public class FriendRequestJsonOut {
    private UUID id;
    private String message;
    private AccountShortJson to;
    private AccountShortJson from;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AccountShortJson getTo() {
        return to;
    }

    public void setTo(AccountShortJson to) {
        this.to = to;
    }

    public AccountShortJson getFrom() {
        return from;
    }

    public void setFrom(AccountShortJson from) {
        this.from = from;
    }
}
