package dto.out;

import java.util.UUID;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class MyCollectionShortJson {
    String name;
    UUID id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
