package dto.out;

import java.util.List;

/**
 * Created by ja on 29.11.15.
 */
public class FriendRequestsJson {
    private List<FriendRequestJsonOut> sent;
    private List<FriendRequestJsonOut> received;

    public List<FriendRequestJsonOut> getSent() {
        return sent;
    }

    public void setSent(List<FriendRequestJsonOut> sent) {
        this.sent = sent;
    }

    public List<FriendRequestJsonOut> getReceived() {
        return received;
    }

    public void setReceived(List<FriendRequestJsonOut> received) {
        this.received = received;
    }
}
