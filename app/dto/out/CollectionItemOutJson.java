package dto.out;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;

import java.util.UUID;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class CollectionItemOutJson {
    UUID id;
    String name;
    String imageUrl;
    UUID collectionId;
    UUID ownerId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    String description;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public UUID getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(UUID collectionId) {
        this.collectionId = collectionId;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
