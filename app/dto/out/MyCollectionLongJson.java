package dto.out;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.UUID;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class MyCollectionLongJson {
    private UUID id;
    private String name;
    private List<CollectionItemOutJson> items;
    private UUID ownerId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String description;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CollectionItemOutJson> getItems() {
        return items;
    }

    public void setItems(List<CollectionItemOutJson> items) {
        this.items = items;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
