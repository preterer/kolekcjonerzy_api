package security;

import com.auth0.jwt.JWTVerifyException;
import controllers.ApplicationController;
import exceptions.AuthorizationException;
import models.Account;
import play.mvc.Controller;
import utils.Constants;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

/**
 * Created by ja on 01.06.15.
 */
public class SecurityUtils {
    public static SecurityUtils INSTANCE = new SecurityUtils();

    private SecurityUtils(){
        super();
    }

    public Account findAccountByToken(String token) throws AuthorizationException {
        try {
            return JWTUtils.INSTANCE.verifyJWT(token).orElseThrow(AuthorizationException::new);
        } catch (IOException | InvalidKeyException | JWTVerifyException | NoSuchAlgorithmException | SignatureException e) {
            throw new AuthorizationException();
        }
    }

    public Account getAccountFromRequest() throws AuthorizationException {
        return findAccountByToken(ApplicationController.getToken());
    }

    public Boolean verifyPassword(Account account, String password) {
        return account.password.equals(password);
    }

    public void setAuthenticationHeader(final Account account) {
        Controller.response().setHeader(Constants.AUTH_TOKEN_HEADER, JWTUtils.INSTANCE.createToken(account));
    }
}
