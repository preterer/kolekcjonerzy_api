package security;

import be.objectify.deadbolt.core.models.Subject;
import be.objectify.deadbolt.java.AbstractDeadboltHandler;
import exceptions.AuthorizationException;
import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;
import utils.Constants;

import java.util.Optional;

import static play.mvc.Controller.response;

/**
 * Created by ja on 29.11.15.
 */
public class Handler extends AbstractDeadboltHandler {
    @Override
    public F.Promise<Result> beforeAuthCheck(Http.Context context) {
        return F.Promise.pure(null);
    }

    @Override
    public Subject getSubject(Http.Context context) {
        return Optional.ofNullable(context.request().getHeader(Constants.AUTH_TOKEN_HEADER))
                .map(token -> {
                    try {
                        return SecurityUtils.INSTANCE.findAccountByToken(token);
                    } catch (AuthorizationException e) {
                        return null;
                    }
                }).filter(account -> account.active)
                .orElse(null);
    }

    @Override
    public F.Promise<Result> onAuthFailure(Http.Context context, String content) {
        return F.Promise.promise(() -> {
            response().setHeader("Access-Control-Allow-Origin", "*");
            response().setHeader("Allow", "*");
            response().setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
            response().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Referer, User-Agent, Authentication");
            return forbidden();
        });
    }
}
