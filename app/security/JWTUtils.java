package security;

import com.auth0.jwt.Algorithm;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTSigner.Options;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import com.google.common.collect.Maps;
import models.Account;
import utils.Constants;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by ja on 15.09.16.
 */
public class JWTUtils {
    private static class TokenFields {
        private static final String ID = "id";
    }

    public static final JWTUtils INSTANCE = new JWTUtils();

    private final JWTSigner jwtSigner;
    private final JWTVerifier jwtVerifier;
    private final Algorithm algorithm;

    private JWTUtils() {
        this.jwtSigner = new JWTSigner(Constants.JWT_SECRET);
        this.jwtVerifier = new JWTVerifier(Constants.JWT_SECRET);
        this.algorithm = Algorithm.HS256; //FIXME: I should check which algorithm I want to use.
    }

    public String createToken(final Account account) {
        final Options options = new Options();
        options.setAlgorithm(algorithm);
        options.setExpirySeconds(Constants.JWT_EXPIRY_SECONDS);
        options.setIssuedAt(true);
        options.setJwtId(true);
        return jwtSigner.sign(getTokenValues(account), options);
    }

    public Optional<Account> verifyJWT(final String token) throws NoSuchAlgorithmException, SignatureException, JWTVerifyException, InvalidKeyException, IOException {
        return Optional.ofNullable(getAccountIdFromToken(token))
                .map(Account::findByIdActive);
    }

    public UUID getAccountIdFromToken(final String token) throws SignatureException, NoSuchAlgorithmException, JWTVerifyException, InvalidKeyException, IOException {
        return UUID.fromString((String) jwtVerifier.verify(token).get(TokenFields.ID));
    }

    private Map<String, Object> getTokenValues(final Account account) {
        final Map<String, Object> map = Maps.newHashMap();
        map.put(TokenFields.ID, account.id.toString());
        return map;
    }
}
