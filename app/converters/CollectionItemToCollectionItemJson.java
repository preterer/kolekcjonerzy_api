package converters;

import dto.out.CollectionItemOutJson;
import models.CollectionItem;

import java.util.function.Function;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class CollectionItemToCollectionItemJson implements Function<CollectionItem, CollectionItemOutJson> {
    public static final CollectionItemToCollectionItemJson INSTANCE = new CollectionItemToCollectionItemJson();

    private CollectionItemToCollectionItemJson(){super();}

    @Override
    public CollectionItemOutJson apply(CollectionItem collectionItem) {
        CollectionItemOutJson json = new CollectionItemOutJson();
        json.setId(collectionItem.id);
        json.setImageUrl(collectionItem.imageUrl);
        json.setName(collectionItem.name);
        json.setOwnerId(collectionItem.account.id);
        json.setCollectionId(collectionItem.myCollection.id);
        json.setDescription(collectionItem.description);
        return json;
    }
}
