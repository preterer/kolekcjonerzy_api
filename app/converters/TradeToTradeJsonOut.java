package converters;

import dto.out.TradeJsonOut;
import models.Trade;

import java.util.function.Function;

/**
 * Created by mbaranowski on 11.06.15.
 */
public class TradeToTradeJsonOut implements Function<Trade, TradeJsonOut> {
    public static final TradeToTradeJsonOut INSTANCE = new TradeToTradeJsonOut();

    private TradeToTradeJsonOut(){super();}

    @Override
    public TradeJsonOut apply(Trade trade) {
        TradeJsonOut tradeJsonOut = new TradeJsonOut();
        tradeJsonOut.setOfferedItem(CollectionItemToCollectionItemJson.INSTANCE.apply(trade.offeredItem));
        tradeJsonOut.setWantedItem(CollectionItemToCollectionItemJson.INSTANCE.apply(trade.wantedItem));
        tradeJsonOut.setId(trade.id);
        tradeJsonOut.setMessage(trade.message);
        return tradeJsonOut;
    }
}
