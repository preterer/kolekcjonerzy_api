package converters;

import dto.out.FriendRequestJsonOut;
import models.FriendRequest;

import java.util.function.Function;

/**
 * Created by ja on 29.11.15.
 */
public class FriendRequestToFriendRequestJsonOut implements Function<FriendRequest, FriendRequestJsonOut> {
    public static final FriendRequestToFriendRequestJsonOut INSTANCE = new FriendRequestToFriendRequestJsonOut();

    private FriendRequestToFriendRequestJsonOut() {
        super();
    }

    @Override
    public FriendRequestJsonOut apply(FriendRequest friendRequest) {
        FriendRequestJsonOut json = new FriendRequestJsonOut();
        json.setFrom(AccountToAccountShortJson.INSTANCE.apply(friendRequest.from));
        json.setTo(AccountToAccountShortJson.INSTANCE.apply(friendRequest.to));
        json.setMessage(friendRequest.message);
        json.setId(friendRequest.id);
        return json;
    }
}
