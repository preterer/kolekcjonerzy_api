package converters;

import dto.out.MyCollectionLongJson;
import models.MyCollection;

import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class MyCollectionToMyCollectionLongJson implements Function<MyCollection, MyCollectionLongJson> {
    public static final MyCollectionToMyCollectionLongJson INSTANCE = new MyCollectionToMyCollectionLongJson();

    private MyCollectionToMyCollectionLongJson(){super();}

    @Override
    public MyCollectionLongJson apply(MyCollection myCollection) {
        MyCollectionLongJson json = new MyCollectionLongJson();
        json.setId(myCollection.id);
        json.setName(myCollection.name);
        json.setItems(myCollection.collectionItems.stream().filter((item) -> item.active).map(CollectionItemToCollectionItemJson.INSTANCE).collect(Collectors.toList()));
        json.setOwnerId(myCollection.account.id);
        json.setDescription(myCollection.description);
        return json;
    }
}
