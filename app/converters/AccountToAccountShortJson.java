package converters;

import dto.out.AccountShortJson;
import models.Account;

import java.util.function.Function;

/**
 * Created by mbaranowski on 11.06.15.
 */
public class AccountToAccountShortJson implements Function<Account, AccountShortJson> {
    public static final AccountToAccountShortJson INSTANCE = new AccountToAccountShortJson();
    private AccountToAccountShortJson(){super();}

    @Override
    public AccountShortJson apply(Account account) {
        AccountShortJson accountShortJson = new AccountShortJson();
        accountShortJson.setId(account.id);
        accountShortJson.setEmail(account.email);
        accountShortJson.setLastName(account.lastName);
        accountShortJson.setName(account.name);
        return accountShortJson;
    }
}
