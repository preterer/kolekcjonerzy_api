package converters;

import dto.out.MyCollectionShortJson;
import models.MyCollection;

import java.util.function.Function;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class MyCollectionToMyCollectionShortJson implements Function<MyCollection, MyCollectionShortJson> {
    public static MyCollectionToMyCollectionShortJson INSTANCE = new MyCollectionToMyCollectionShortJson();

    private MyCollectionToMyCollectionShortJson(){super();}
    @Override
    public MyCollectionShortJson apply(MyCollection myCollection) {
        MyCollectionShortJson myCollectionShortJson = new MyCollectionShortJson();
        myCollectionShortJson.setId(myCollection.id);
        myCollectionShortJson.setName(myCollection.name);
        return myCollectionShortJson;
    }
}
