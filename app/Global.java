import com.google.common.collect.Lists;
import models.Account;
import models.Role;
import play.Application;
import play.GlobalSettings;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import utils.Constants;
import utils.CorsUtils;
import utils.PreparationUtils;


import java.util.List;
import java.util.Optional;

import static play.mvc.Controller.response;
import static play.mvc.Results.badRequest;
import static play.mvc.Results.internalServerError;

/**
 * Created by ja on 29.11.15.
 */
public class Global extends GlobalSettings {


    @Override
    public void onStart(Application application) {
        PreparationUtils.makeSureRolesExist();
        PreparationUtils.makeSureAdminAccountExists();
        //PreparationUtils.makeSureRandomCollectionsExist();
    }

    // For CORS
    private class ActionWrapper extends Action.Simple {
        public ActionWrapper(Action<?> action) {
            this.delegate = action;
        }

        @Override
        public F.Promise<Result> call(Http.Context ctx) throws java.lang.Throwable {
            F.Promise<Result> result = this.delegate.call(ctx);
            CorsUtils.setDefaultHeaders(ctx.response());
            return result;
        }
    }

    @Override
    public Action<?> onRequest(Http.Request request, java.lang.reflect.Method actionMethod) {
        return new ActionWrapper(super.onRequest(request, actionMethod));
    }

    @Override
    public F.Promise<Result> onError(Http.RequestHeader requestHeader, Throwable throwable) {
        return F.Promise.promise(() -> {
            CorsUtils.setDefaultHeaders(response());
            return internalServerError();
        });
    }
}
