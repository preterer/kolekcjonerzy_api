package controllers;

import com.google.common.collect.Lists;
import converters.AccountToAccountShortJson;
import converters.MyCollectionToMyCollectionShortJson;
import dto.in.SearchQueryJson;
import dto.out.SearchResultJson;
import models.Account;
import models.MyCollection;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import utils.JsonUtils;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by ja on 29.11.15.
 */
public class SearchController extends Controller {
    public static F.Promise<Result> search() {
        return F.Promise.promise(() -> {
            SearchQueryJson json = JsonUtils.getJsonFromRequest(SearchQueryJson.class);
            SearchResultJson out = new SearchResultJson();
            out.setAccounts(Optional.ofNullable(json)
                    .filter(verifiedJson -> verifiedJson.getLimit() != null)
                    .filter(verifiedJson -> verifiedJson.getStart() != null)
                    .map(verifiedJson -> Account.findByNamesAndLastNamesLimited(verifiedJson.getQuery(), verifiedJson.getLimit(), verifiedJson.getStart()))
                    .orElseGet(() -> Optional.ofNullable(Account.findByNamesAndLastNames(json.getQuery())).orElseGet(Lists::newArrayList))
                    .stream().map(AccountToAccountShortJson.INSTANCE).collect(Collectors.toList()));

            out.setCollections(Optional.ofNullable(json)
                    .filter(verifiedJson -> verifiedJson.getLimit() != null)
                    .filter(verifiedJson -> verifiedJson.getStart() != null)
                    .map(verifiedJson -> MyCollection.findByNameLikeLimited(verifiedJson.getQuery(), verifiedJson.getLimit(), verifiedJson.getStart()))
                    .orElseGet(() -> Optional.ofNullable(MyCollection.findByNameLike(json.getQuery())).orElseGet(Lists::newArrayList))
                    .stream().map(MyCollectionToMyCollectionShortJson.INSTANCE).collect(Collectors.toList()));

            return ok(Json.toJson(out));
        });
    }
}
