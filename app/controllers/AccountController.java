package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.wordnik.swagger.annotations.*;
import converters.AccountToAccountShortJson;
import dto.in.ChangePasswordJson;
import dto.in.EditAccountJson;
import dto.out.AccountShortJson;
import exceptions.AuthorizationException;
import models.Account;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.SecurityUtils;
import utils.JsonUtils;

import java.util.Optional;
import java.util.UUID;


/**
 * Created by ja on 29.11.15.
 */
@Api(value = "/me", description = "Operations on account", consumes = "application/json", produces = "application/json")
public class AccountController extends Controller {

    @ApiOperation(value = "Get current account", notes = "Returns account associated with sent token", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns account", response = AccountShortJson.class),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> getSelf() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return getAccount(account.id);
    }


    @ApiOperation(value = "Get user account", notes = "Returns account found by given ID", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "User ID", value = "Id of the user to find", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns account", response = AccountShortJson.class),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Account not found")
    })
    @SubjectPresent
    public static F.Promise<Result> getAccount(UUID userId) {
        return F.Promise.promise(() -> Optional.ofNullable(Account.findByIdActive(userId))
                        .map(AccountToAccountShortJson.INSTANCE)
                        .map(json -> ok(Json.toJson(json)))
                        .orElseGet(() -> badRequest(""))
        );
    }


    @ApiOperation(value = "Edit current account", notes = "Edits current account", httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "body", value = "Account data to save", paramType = "body", dataType = "EditAccountJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns edited account", response = AccountShortJson.class),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> editSelf() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        EditAccountJson json = JsonUtils.getJsonFromRequest(EditAccountJson.class);
        return F.Promise.promise(() -> {
            account.name = Optional.ofNullable(json.getName()).orElse(account.name);
            account.lastName = Optional.ofNullable(json.getLastName()).orElse(account.lastName);
            account.update();
            return ok(Json.toJson(AccountToAccountShortJson.INSTANCE.apply(account)));
        });
    }


    @ApiOperation(value = "Change current accounts password", notes = "Returns account associated with sent token", httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "body", value = "Old and new password", paramType = "body", dataType = "ChangePasswordJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Password changed"),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> changePassword() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        ChangePasswordJson json = JsonUtils.getJsonFromRequest(ChangePasswordJson.class);
        return F.Promise.promise(() -> {
            if (SecurityUtils.INSTANCE.verifyPassword(account, json.getOldPassword())) {
                account.password = json.getNewPassword();
                account.update();
                return noContent();
            }
            return forbidden();
        });
    }


    @ApiOperation(value = "Disables current account", notes = "Disables current account permanently", httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Account disabled"),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> disableAccount() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> {
            account.active = false;
            account.update();
            return noContent();
        });
    }
}
