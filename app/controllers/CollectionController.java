package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.wordnik.swagger.annotations.*;
import converters.MyCollectionToMyCollectionLongJson;
import converters.MyCollectionToMyCollectionShortJson;
import dto.in.CollectionJson;
import dto.out.MyCollectionLongJson;
import exceptions.AuthorizationException;
import models.Account;
import models.CollectionItem;
import models.MyCollection;
import play.db.ebean.Transactional;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.SecurityUtils;
import utils.JsonUtils;
import utils.TradeUtils;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by mbaranowski on 01.06.15.
 */

@Api(value = "/collection", description = "Collection operations", consumes = "application/json", produces = "application/json")
public class CollectionController extends Controller {

    @ApiOperation(value = "Creates a collection", notes = "Creates a new collection if given name is not in use", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "body", value = "Collection data", paramType = "body", dataType = "CollectionJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Collection created", response = MyCollectionLongJson.class),
            @ApiResponse(code = 400, message = "Invalid json, or name in use"),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    @Transactional
    public static F.Promise<Result> addCollection() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        CollectionJson collectionJson = JsonUtils.getJsonFromRequest(CollectionJson.class);
        return F.Promise.promise(() -> Optional.ofNullable(MyCollection.findByNameAndUserIdActive(collectionJson.getName(), account.id))
                .map(collection -> badRequest())
                .orElseGet(() -> {
                    MyCollection collection = new MyCollection();
                    collection.name = collectionJson.getName();
                    collection.description = collectionJson.getDescription();
                    collection.account = account;
                    collection.save();
                    Optional.ofNullable(collectionJson.getItems())
                            .ifPresent(items -> items
                                    .forEach((itemJson) -> {
                                        CollectionItem item = new CollectionItem();
                                        item.name = itemJson.getName();
                                        item.imageUrl = itemJson.getImageUrl();
                                        item.description = itemJson.getDescription();
                                        item.myCollection = collection;
                                        item.account = account;
                                        item.save();
                                    }));
                    return ok(Json.toJson(MyCollectionToMyCollectionLongJson.INSTANCE.apply(collection)));
                }));
    }

    @ApiOperation(value = "Edits a collection", notes = "Edits a collection if given name is not in use", httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "id", value = "Collection id", paramType = "path", dataType = "string", required = true),
            @ApiImplicitParam(name = "body", value = "Collection data", paramType = "body", dataType = "CollectionJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Collection edited", response = MyCollectionLongJson.class),
            @ApiResponse(code = 400, message = "Invalid json, or name in use"),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Collection not found")
    })
    @SubjectPresent
    public static F.Promise<Result> editCollection(UUID collectionId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        CollectionJson json = JsonUtils.getJsonFromRequest(CollectionJson.class);
        return F.Promise.promise(() -> Optional.ofNullable(MyCollection.findByIdAndUserIdActive(collectionId, account.id))
                        .map(collection -> Optional.ofNullable(MyCollection.findByNameAndUserIdActive(json.getName(), account.id))
                                .map(collectionWithNewName -> badRequest("collection name taken"))
                                .orElseGet(() -> {
                                    collection.name = Optional.ofNullable(json.getName()).orElse(collection.name);
                                    collection.description = Optional.ofNullable(json.getDescription()).orElse(collection.description);
                                    collection.update();
                                    return ok(Json.toJson(MyCollectionToMyCollectionLongJson.INSTANCE.apply(collection)));
                                }))
                        .orElseGet(Results::notFound)
        );
    }

    @ApiOperation(value = "Delete a collection", notes = "Deletes a collection", httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "id", value = "Collection id", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Collection deleted", response = MyCollectionLongJson.class),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Collection not found")
    })
    @SubjectPresent
    public static F.Promise<Result> removeCollection(UUID collectionId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(MyCollection.findByIdAndUserIdActive(collectionId, account.id))
                .map(collection -> {
                    collection.active = false;
                    collection.update();
                    collection.collectionItems.stream().forEach(item -> {
                        item.active = false;
                        item.update();
                        TradeUtils.clearItemTrades(item.id);
                    });
                    return collection;
                }).map(collection -> noContent())
                .orElseGet(Results::notFound));
    }

    @ApiOperation(value = "Get all user collections", notes = "Gets all user collections, by id of the user", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "id", value = "User ID", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Collections of the user", response = MyCollectionLongJson.class),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @SubjectPresent
    public static F.Promise<Result> getUserCollections(UUID userId) {
        return F.Promise.promise(() -> Optional.ofNullable(Account.findByIdActive(userId))
                        .map(MyCollection::findActiveByUser)
                        .map(Collection::stream)
                        .map(stream -> stream.map(MyCollectionToMyCollectionLongJson.INSTANCE))
                        .map(stream -> stream.collect(Collectors.toList()))
                        .map(Json::toJson)
                        .map(Results::ok)
                        .orElseGet(Results::badRequest)
        );
    }

    @ApiOperation(value = "Get all current user collections", notes = "Gets all current user collections", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Collections of the user", response = MyCollectionLongJson.class),
            @ApiResponse(code = 401, message = "No token present"),
    })
    @SubjectPresent
    public static F.Promise<Result> getCurrentUserCollections() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return getUserCollections(account.id);
    }

    @ApiOperation(value = "Get all collections", notes = "Gets all collections", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Collections of the user", response = MyCollectionLongJson.class),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> getCollections() {
        return F.Promise.promise(() -> ok(Json.toJson(MyCollection.findActive().stream()
                .map(MyCollectionToMyCollectionShortJson.INSTANCE)
                .collect(Collectors.toList()))));
    }

    @ApiOperation(value = "Get collection", notes = "Get a collection by ID", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Collection ID", value = "Id of the collection", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found collection", response = MyCollectionLongJson.class),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Collection not found")
    })
    @SubjectPresent
    public static F.Promise<Result> getCollection(UUID collectionId) {
        return F.Promise.promise(() -> Optional.ofNullable(MyCollection.findActiveById(collectionId.toString()))
                .map(MyCollectionToMyCollectionLongJson.INSTANCE)
                .map(json -> ok(Json.toJson(json)))
                .orElseGet(Results::badRequest));
    }

    @ApiOperation(value = "Get random collection", notes = "Gets a predefined amount of random collections", httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Random collections", response = MyCollectionLongJson.class)
    })
    public static F.Promise<Result> getRandomCollections() {
        return F.Promise.promise(() -> ok(Json.toJson(MyCollection.findRandom().stream()
                .map(MyCollectionToMyCollectionLongJson.INSTANCE)
                .collect(Collectors.toList()))));
    }

}
