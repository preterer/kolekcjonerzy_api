package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.wordnik.swagger.annotations.*;
import converters.TradeToTradeJsonOut;
import dto.in.TradeJson;
import dto.out.CollectionItemOutJson;
import dto.out.TradeJsonOut;
import exceptions.AuthorizationException;
import models.*;
import play.db.ebean.Transactional;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.SecurityUtils;
import utils.JsonUtils;
import utils.TradeUtils;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by mbaranowski on 03.06.15.
 */

@Api(value = "/item", description = "Collection item operations", consumes = "application/json", produces = "application/json")
public class TradeController extends Controller {


    @ApiOperation(value = "Create trade", notes = "Creates a new trade between item A and item B", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "body", value = "Trade data", paramType = "body", dataType = "TradeJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Trade created", response = TradeJsonOut.class),
            @ApiResponse(code = 400, message = "Invalid json, or item not found, or items owned by the same user"),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> createTrade() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        TradeJson tradeJson = JsonUtils.getJsonFromRequest(TradeJson.class);
        return F.Promise.promise(() -> Optional.ofNullable(Trade.findByTradeItemsActive(tradeJson.getOfferedItemId(), tradeJson.getWantedItemId()))
                .map(trade -> badRequest())
                .orElseGet(() -> {
                    CollectionItem offeredItem = CollectionItem.findByIdAndUserIdActive(tradeJson.getOfferedItemId(), account.id);
                    CollectionItem wantedItem = CollectionItem.findByIdNotOwnedByUserActive(tradeJson.getWantedItemId(), account.id);
                    if (offeredItem != null && wantedItem != null && !offeredItem.account.id.equals(wantedItem.account.id)) {
                        Trade trade = new Trade();
                        trade.message = tradeJson.getMessage();
                        trade.offeredItem = offeredItem;
                        trade.wantedItem = wantedItem;
                        trade.save();
                        return ok(Json.toJson(TradeToTradeJsonOut.INSTANCE.apply(trade)));
                    }
                    return badRequest();
                })
        );
    }

    @ApiOperation(value = "Accept trade", notes = "Moves items owners and collections and disables trade", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Trade ID", value = "Trade Id", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Trade accepted"),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Trade not found")
    })
    @SubjectPresent
    @Transactional
    public static F.Promise<Result> acceptTrade(UUID tradeId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(Trade.findByIdAndWantedItemUserIdActive(tradeId, account.id))
                .map(trade -> {
                    //Have to keep backup of those, cause they'd otherwise change
                    MyCollection wantedItemCollection = trade.wantedItem.myCollection;
                    MyCollection offeredItemCollection = trade.offeredItem.myCollection;
                    Account wantedItemOwner = trade.wantedItem.account;
                    Account offeredItemOwner = trade.offeredItem.account;
                    //^
                    trade.offeredItem.account = wantedItemOwner;
                    trade.offeredItem.myCollection = wantedItemCollection;
                    trade.offeredItem.update();
                    trade.wantedItem.account = offeredItemOwner;
                    trade.wantedItem.myCollection = offeredItemCollection;
                    trade.wantedItem.update();
                    trade.active = false;
                    trade.update();
                    TradeUtils.disableTradesOfTradeItems(trade);
                    return trade;
                }).map(trade -> noContent())
                .orElseGet(Results::notFound));
    }


    @ApiOperation(value = "Reject trade", notes = "Disables trade", httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Trade ID", value = "Trade Id", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Trade rejected"),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Trade not found")
    })
    @SubjectPresent
    public static F.Promise<Result> rejectTrade(UUID tradeId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(Trade.findByIdAndWantedItemUserIdActive(tradeId, account.id))
                .map(trade -> {
                    trade.active = false;
                    trade.update();
                    return trade;
                }).map(trade -> noContent())
                .orElseGet(Results::forbidden));
    }


    @ApiOperation(value = "Get offers", notes = "Gets offers of the current user", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of offers", response = TradeJsonOut.class),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> getOffers() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> {
            List<TradeJsonOut> trades = Trade.findOffersByAccountActive(account.id).stream().map(TradeToTradeJsonOut.INSTANCE).collect(Collectors.toList());
            return ok(Json.toJson(trades));
        });
    }

    @ApiOperation(value = "Get requests", notes = "Gets trade requests of the current user", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of trade requests", response = TradeJsonOut.class),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> getRequests() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> {
            List<TradeJsonOut> trades = Trade.findRequestsByAccountActive(account.id).stream().map(TradeToTradeJsonOut.INSTANCE).collect(Collectors.toList());
            return ok(Json.toJson(trades));
        });
    }
}
