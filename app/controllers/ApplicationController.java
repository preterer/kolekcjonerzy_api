package controllers;

import exceptions.AuthorizationException;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import utils.Constants;

/**
 * Created by mbaranowski on 02.06.15.
 */
public class ApplicationController extends Controller {

    public static F.Promise<Result> options(String anyRoute) {
        return F.Promise.promise(Results::ok);
    }

    public static String getToken() throws AuthorizationException {
        if (request().hasHeader(Constants.AUTH_TOKEN_HEADER)) {
            return request().getHeader(Constants.AUTH_TOKEN_HEADER);
        } else {
            throw new AuthorizationException();
        }
    }
}
