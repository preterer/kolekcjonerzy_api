package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.google.common.collect.Lists;
import com.wordnik.swagger.annotations.*;
import converters.AccountToAccountShortJson;
import converters.FriendRequestToFriendRequestJsonOut;
import dto.in.MessageJson;
import dto.out.AccountShortJson;
import dto.out.FriendRequestsJson;
import dto.out.MyCollectionLongJson;
import exceptions.AuthorizationException;
import models.Account;
import models.FriendRequest;
import models.FriendsList;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.SecurityUtils;
import utils.JsonUtils;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by ja on 29.11.15.
 */
@Api(value = "/friends", description = "Friends operations", consumes = "application/json", produces = "application/json")
public class FriendsController extends Controller {
    @ApiOperation(value = "Send friends request", notes = "Creates a friend request", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "User ID", value = "Id of the user", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Friends request created"),
            @ApiResponse(code = 400, message = "Already friends or a friend request exists"),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "User not found")
    })
    @SubjectPresent
    public static F.Promise<Result> sendFriendsRequest(UUID userId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> {
                    if (account.id.equals(userId)) {
                        return badRequest();
                    }
                    return Optional.ofNullable(Account.findByIdActive(userId))
                            .filter(user -> FriendRequest.findByFromOrTo(user, account) == null)
                            .filter(user -> FriendsList.findByOwnerAndFriend(account, user) == null)
                            .map(user -> {
                                MessageJson json = JsonUtils.getJsonFromRequest(MessageJson.class);
                                FriendRequest friendRequest = new FriendRequest();
                                friendRequest.active = true;
                                friendRequest.from = account;
                                friendRequest.to = user;
                                friendRequest.message = json.getMessage();
                                friendRequest.save();
                                return friendRequest;
                            }).map(friendRequest -> noContent())
                            .orElseGet(Results::badRequest);
                }
        );
    }

    @ApiOperation(value = "Accept friends request", notes = "Adds users to friends lists, disables friends reqest", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Request ID", value = "Id of the request", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Friends request accepted"),
            @ApiResponse(code = 400, message = "Friends request not found or trying to accept someone elses friends request"),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> acceptFriendsRequest(UUID requestId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(FriendRequest.findByIdAndUserToActive(requestId, account.id))
                        .map(friendRequest -> {
                            account.friends.friends.add(friendRequest.from);
                            account.friends.update();
                            friendRequest.from.friends.friends.add(account);
                            friendRequest.from.friends.update();
                            friendRequest.active = false;
                            friendRequest.update();
                            return noContent();
                        }).orElseGet(Results::badRequest)
        );
    }

    @ApiOperation(value = "Reject friends request", notes = "Disables a friend request", httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Request ID", value = "Id of the request", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Friends request accepted"),
            @ApiResponse(code = 400, message = "Friends request not found or trying to reject someone elses friends request"),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> rejectFriendsRequest(UUID requestId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(FriendRequest.findByIdAndUserToActive(requestId, account.id))
                        .map(request -> {
                            request.active = false;
                            request.update();
                            return noContent();
                        }).orElseGet(Results::notFound)
        );
    }


    @ApiOperation(value = "Get friends request", notes = "Gets users friends requests", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of friends requests", response = FriendRequestsJson.class),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> getFriendsRequests() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> {
            List<FriendRequest> sentRequests = Optional.ofNullable(FriendRequest.findByFromActive(account)).orElseGet(Lists::newArrayList);
            List<FriendRequest> receivedRequests = Optional.ofNullable(FriendRequest.findByToActive(account)).orElseGet(Lists::newArrayList);
            FriendRequestsJson json = new FriendRequestsJson();
            json.setSent(sentRequests.stream().map(FriendRequestToFriendRequestJsonOut.INSTANCE).collect(Collectors.toList()));
            json.setReceived(receivedRequests.stream().map(FriendRequestToFriendRequestJsonOut.INSTANCE).collect(Collectors.toList()));
            return ok(Json.toJson(json));
        });
    }

    @ApiOperation(value = "Get friends request with paging", notes = "Gets users friends requests with paging", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "limit", value = "How many friends requests to take", paramType = "path", dataType = "integer", required = true),
            @ApiImplicitParam(name = "start", value = "Number of friends requests to skip", paramType = "path", dataType = "integer", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of friends requests", response = FriendRequestsJson.class),
            @ApiResponse(code = 401, message = "No token present")
    })
    @SubjectPresent
    public static F.Promise<Result> getFriendsRequestsLimited(Integer limit, Integer start) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> {
            List<FriendRequest> sentRequests = Optional.ofNullable(FriendRequest.findByFromWithLimitActive(account, limit, start)).orElseGet(Lists::newArrayList);
            List<FriendRequest> receivedRequests = Optional.ofNullable(FriendRequest.findByToWithLimitActive(account, limit, start)).orElseGet(Lists::newArrayList);
            FriendRequestsJson json = new FriendRequestsJson();
            json.setSent(sentRequests.stream().map(FriendRequestToFriendRequestJsonOut.INSTANCE).collect(Collectors.toList()));
            json.setReceived(receivedRequests.stream().map(FriendRequestToFriendRequestJsonOut.INSTANCE).collect(Collectors.toList()));
            return ok(Json.toJson(json));
        });
    }

    @ApiOperation(value = "Get friends", notes = "Gets friends list of current user", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of friends", response = AccountShortJson.class),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @SubjectPresent
    public static F.Promise<Result> getFriends() throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> ok(Json.toJson(account.friends.friends.stream()
                .filter(friend -> friend.active)
                .map(AccountToAccountShortJson.INSTANCE)
                .collect(Collectors.toList()))));
    }

    @ApiOperation(value = "Get other user friends", notes = "Gets friends list of a user by user Id", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "User id", value = "Id of the user", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of friends", response = AccountShortJson.class),
            @ApiResponse(code = 401, message = "Unauthorized")
    })
    @SubjectPresent
    public static F.Promise<Result> getOtherUserFriends(UUID userId) {
        return F.Promise.promise(() -> Optional.ofNullable(Account.findByIdActive(userId))
                        .map(account -> account.friends.friends)
                        .map(Collection::stream)
                        .map(stream -> stream.filter(account -> account.active))
                        .map(stream -> stream.map(AccountToAccountShortJson.INSTANCE))
                        .map(stream -> stream.collect(Collectors.toList()))
                        .map(json -> ok(Json.toJson(json)))
                        .orElseGet(Results::badRequest)
        );
    }
}
