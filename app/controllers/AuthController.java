package controllers;

import be.objectify.deadbolt.java.actions.SubjectNotPresent;
import dto.in.SignInJson;
import dto.in.SignUpJson;
import com.wordnik.swagger.annotations.*;
import models.Account;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.SecurityUtils;
import utils.JsonUtils;

import java.util.Optional;

/**
 * Created by mbaranowski on 02.06.15.
 */

@Api(value = "/auth", description = "Basic auth operations", consumes = "application/json", produces = "application/json")
public class AuthController extends Controller {

    @ApiOperation(value = "Create an account", notes = "Creates a new account if given e-mail is not used already and sets up authentication header", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "body", value = "User data", paramType = "body", dataType = "SignUpJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Account created and set up authentication header"),
            @ApiResponse(code = 400, message = "Invalid json"),
            @ApiResponse(code = 403, message = "Email is in use")
    })
    @SubjectNotPresent
    public static F.Promise<Result> signUp() {
        SignUpJson signUpJson = JsonUtils.getJsonFromRequest(SignUpJson.class);
        return F.Promise.promise(() -> Optional.ofNullable(Account.findByEmailActive(signUpJson.getEmail()))
                .map(account -> forbidden("Email already exists"))
                .orElseGet(() -> {
                    SecurityUtils.INSTANCE.setAuthenticationHeader(Account.getNewAccount(signUpJson));
                    return noContent();
                }));
    }

    @ApiOperation(value = "Log in", notes = "Returns authentication header if given authentication data is correct", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "body", value = "User login data", paramType = "body", dataType = "SignInJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Successfully logged in, and set up authentication header"),
            @ApiResponse(code = 400, message = "Invalid json"),
            @ApiResponse(code = 401, message = "Invalid data")
    })
    @SubjectNotPresent
    public static F.Promise<Result> signIn() {
        SignInJson signInJson = JsonUtils.getJsonFromRequest(SignInJson.class);
        return F.Promise.promise(() -> Optional.ofNullable(Account.findByLoginDataActive(signInJson.getEmail(), signInJson.getPassword()))
                .map(account -> {
                    SecurityUtils.INSTANCE.setAuthenticationHeader(account);
                    return noContent();
                }).orElseGet(Results::unauthorized));
    }

}
