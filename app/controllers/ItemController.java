package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.wordnik.swagger.annotations.*;
import converters.CollectionItemToCollectionItemJson;
import dto.in.CollectionItemJson;
import dto.out.CollectionItemOutJson;
import exceptions.AuthorizationException;
import models.Account;
import models.CollectionItem;
import models.MyCollection;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import security.SecurityUtils;
import utils.JsonUtils;
import utils.TradeUtils;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */

@Api(value = "/item", description = "Collection item operations", consumes = "application/json", produces = "application/json")
public class ItemController extends Controller {

    @ApiOperation(value = "Add item", notes = "Adds a new item to an existing collection", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Collection ID", value = "Id of the collection", paramType = "path", dataType = "string", required = true),
            @ApiImplicitParam(name = "body", value = "Items data", paramType = "body", dataType = "CollectionItemJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Item created", response = CollectionItemOutJson.class),
            @ApiResponse(code = 400, message = "Invalid json"),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Collection not found")
    })
    @SubjectPresent
    public static F.Promise<Result> addItem(UUID collectionId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(MyCollection.findByIdAndUserIdActive(collectionId, account.id))
                .map(collection -> {
                    CollectionItemJson itemJson = JsonUtils.getJsonFromRequest(CollectionItemJson.class);
                    CollectionItem item = new CollectionItem();
                    item.name = itemJson.getName();
                    item.imageUrl = itemJson.getImageUrl();
                    item.description = itemJson.getDescription();
                    item.myCollection = collection;
                    item.account = account;
                    item.save();
                    return item;
                }).map(CollectionItemToCollectionItemJson.INSTANCE)
                .map(json -> ok(Json.toJson(json)))
                .orElseGet(Results::notFound));
    }


    @ApiOperation(value = "Remove item", notes = "Removes an item", httpMethod = "DELETE")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Item ID", value = "Id of the collection", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 204, message = "Item deleted", response = CollectionItemOutJson.class),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Item not found")
    })
    @SubjectPresent
    public static F.Promise<Result> removeItem(UUID itemId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(CollectionItem.findByIdAndUserIdActive(itemId, account.id))
                        .map(item -> {
                            item.active = false;
                            item.update();
                            TradeUtils.clearItemTrades(item.id);
                            return item;
                        }).map(item -> noContent())
                        .orElseGet(Results::notFound)
        );
    }

    @ApiOperation(value = "Edit item", notes = "Edits an item", httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Item ID", value = "Id of the collection", paramType = "path", dataType = "string", required = true),
            @ApiImplicitParam(name = "body", value = "Items data", paramType = "body", dataType = "CollectionItemJson", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Edited item", response = CollectionItemOutJson.class),
            @ApiResponse(code = 400, message = "Invalid json"),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Item not found")
    })
    @SubjectPresent
    public static F.Promise<Result> editItem(UUID itemId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(CollectionItem.findByIdAndUserIdActive(itemId, account.id))
                        .map(item -> {
                            CollectionItemJson itemJson = JsonUtils.getJsonFromRequest(CollectionItemJson.class);
                            item.name = Optional.ofNullable(itemJson.getName()).orElse(item.name);
                            item.imageUrl = Optional.ofNullable(itemJson.getImageUrl()).orElse(item.imageUrl);
                            item.description = Optional.ofNullable(itemJson.getDescription()).orElse(item.description);
                            item.update();
                            return item;
                        }).map(CollectionItemToCollectionItemJson.INSTANCE)
                        .map(json -> ok(Json.toJson(json)))
                        .orElseGet(Results::notFound)
        );
    }


    @ApiOperation(value = "Move item", notes = "Moves an item from collection A to collection B, owned by the same user", httpMethod = "PUT")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authentication", value = "Authentication token", paramType = "header", dataType = "string", required = true),
            @ApiImplicitParam(name = "Item Id", value = "Id of the item", paramType = "path", dataType = "string", required = true),
            @ApiImplicitParam(name = "Collection ID", value = "Id of the collection, to move item to", paramType = "path", dataType = "string", required = true)
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Item moved", response = CollectionItemOutJson.class),
            @ApiResponse(code = 401, message = "No token present"),
            @ApiResponse(code = 404, message = "Collection, or item not found")
    })
    @SubjectPresent
    public static F.Promise<Result> moveItem(UUID itemId, UUID collectionId) throws AuthorizationException {
        Account account = SecurityUtils.INSTANCE.getAccountFromRequest();
        return F.Promise.promise(() -> Optional.ofNullable(CollectionItem.findByIdAndUserIdActive(itemId, account.id))
                        .flatMap(item -> Optional.ofNullable(MyCollection.findByIdAndUserIdActive(collectionId, account.id))
                                .map(myCollection -> {
                                    item.myCollection = myCollection;
                                    item.update();
                                    return item;
                                })
                        ).map(CollectionItemToCollectionItemJson.INSTANCE)
                        .map(json -> ok(Json.toJson(json)))
                        .orElseGet(Results::notFound)
        );
    }
}
