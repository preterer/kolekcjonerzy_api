package utils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.libs.Json;
import play.mvc.Controller;

import java.io.IOException;


/**
 * Created by kchudy on 05.03.15.
 */
public class JsonUtils {

    public static <T> T getJsonFromRequest(Class<T> clazz) {
        JsonNode node = Controller.request().body().asJson();
        return Json.fromJson(node, clazz);

    }

    public static <T> T getJson(String string, Class<T> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser jp = factory.createParser(string);
        JsonNode node = mapper.readTree(jp);
        return Json.fromJson(node, clazz);
    }

    public static JsonNode getJson(String string) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser jp = factory.createParser(string);
        return mapper.readTree(jp);
    }
}