package utils;

import com.google.common.collect.Lists;
import models.Account;
import models.MyCollection;
import models.Role;
import play.Logger;

import java.util.List;
import java.util.Optional;

/**
 * Created by ja on 30.12.15.
 */
public class PreparationUtils {
    private static Role createRole(String name) {
        Logger.debug("Creating role " + name);
        Role role = new Role();
        role.name = name;
        role.save();
        return role;
    }

    private static Account createMainAccount() {
        Logger.debug("Creating admin account");
        Account account = new Account();
        account.active = true;
        account.email = "test@test.pl";
        account.password = "password";
        account.name = "John";
        account.lastName = "Doe";
        account.roles = Lists.newArrayList(Optional.ofNullable(Role.findByName("admin")).orElseGet(() -> createRole("admin")));
        account.save();
        return account;
    }

    private static List<MyCollection> createCollections() {
        Logger.debug("Creating collections");
        List<MyCollection> collections = Lists.newArrayList();
        Account account = Account.findByEmail("test@test.pl");
        for (int i = 0; i < 100000; i++) {
            MyCollection myCollection = new MyCollection();
            myCollection.account = account;
            myCollection.collectionItems = Lists.newArrayList();
            myCollection.description = "Opis";
            myCollection.name = "Kolekcja " + i;
            myCollection.save();
            collections.add(myCollection);
        }
        return collections;
    }

    public static void makeSureAdminAccountExists() {
        Optional.ofNullable(Account.findByEmailActive("test@test.pl"))
                .orElseGet(PreparationUtils::createMainAccount);
    }

    public static void makeSureRolesExist() {
        List<String> roles = Lists.newArrayList("admin", "user");
        roles.stream().forEach(roleName -> Optional.ofNullable(Role.findByName(roleName)).orElseGet(() -> createRole(roleName)));
    }

    public static void makeSureRandomCollectionsExist() {
        Optional.ofNullable(MyCollection.findActive()).filter(myCollections -> !myCollections.isEmpty()).orElseGet(PreparationUtils::createCollections);
    }
}
