package utils;

import models.Trade;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by ja on 29.11.15.
 */
public class TradeUtils {
    public static void clearItemTrades(UUID itemId){
        Optional.ofNullable(Trade.findActiveByItemId(itemId))
                .ifPresent(trades -> trades.stream()
                        .forEach(trade -> {
                            trade.active = false;
                            trade.update();
                        }));
    }

    public static void disableTradesOfTradeItems(Trade trade) {
        clearItemTrades(trade.offeredItem.id);
        clearItemTrades(trade.wantedItem.id);
    }
}
