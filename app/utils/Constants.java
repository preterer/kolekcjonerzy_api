package utils;

import com.google.common.collect.Lists;
import play.Play;

import java.util.List;
import java.util.Optional;

/**
 * Created by ja on 29.11.15.
 */
public class Constants {
    /**
     * Roles
     */

    public static final String USER_ROLE = "user";
    public static final String ADMIN_ROLE = "admin";

    public static final List<String> ROLES = Lists.newArrayList(USER_ROLE, ADMIN_ROLE);


    /**
     * Security
     */
    private static final String DEFAULT_AUTH_TOKEN_HEADER = "Authentication";
    private static final String DEFAULT_SECRET_KEY = "secret";
    private static final Integer DEFAULT_EXPIRY_SECONDS = 60 * 60; //1h

    public static final String AUTH_TOKEN_HEADER = Optional.ofNullable(Play.application().configuration().getString("token.header")).orElse(DEFAULT_AUTH_TOKEN_HEADER);
    public static final String JWT_SECRET = Optional.ofNullable(Play.application().configuration().getString("token.secret")).orElse(DEFAULT_SECRET_KEY);
    public static final Integer JWT_EXPIRY_SECONDS = Optional.ofNullable(Play.application().configuration().getInt("token.expiry")).orElse(DEFAULT_EXPIRY_SECONDS);

    /**
     * Randomness generation
     */

    public static final int RANDOM_COLLECTIONS_AMOUNT = Optional.ofNullable(Play.application().configuration().getInt("collections.amount")).orElse(5);
}
