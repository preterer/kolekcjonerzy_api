package utils;

import play.mvc.Http;


/**
 * Created by ja on 12.01.16.
 */
public class CorsUtils {
    public static void setDefaultHeaders(Http.Response response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Allow", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Referer, User-Agent, " + Constants.AUTH_TOKEN_HEADER);
        response.setHeader("Access-Control-Expose-Headers", Constants.AUTH_TOKEN_HEADER);
    }
}
